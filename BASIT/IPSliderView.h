//
//  IPSliderView.h
//  Ajnad
//
//  Created by Haitham on 12/15/14.
//  Copyright (c) 2014 iPhoneAlsham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPSliderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *sliderImg;
@property (weak, nonatomic) IBOutlet UIImageView *AvatarImg;
@property (weak, nonatomic) IBOutlet UIImageView *FlagImg;

@property (weak, nonatomic) IBOutlet UILabel *sliderTitle;
@property (weak, nonatomic) IBOutlet UILabel *NameTitle;

@property (weak, nonatomic) IBOutlet UILabel *sliderRate;

@end
