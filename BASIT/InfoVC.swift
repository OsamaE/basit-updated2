//
//  InfoVC.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class InfoVC: SuperViewController {
    var Info : AboutUs?
    var IsImage = true
    @IBOutlet weak var uiweb_view: UIWebView!
    @IBOutlet weak var image_view: UIView!
    @IBOutlet weak var image_about: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle(Info?.title as! NSString, sender: self)
        self.uiweb_view.loadHTMLString((self.Info?.description!)!, baseURL: Bundle.main.bundleURL)
        if !IsImage {
            image_view.isHidden = true
        }else{
            self.image_about.sd_custom(url: (self.Info?.image!)!)

        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
