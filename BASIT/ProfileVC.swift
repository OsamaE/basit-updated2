//
//  ProfileVC.swift
//  BASIT
//
//  Created by ahmed on 2/25/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var lb_mobile: UILabel!
    @IBOutlet weak var lb_country: UILabel!
    @IBOutlet weak var lb_location: UILabel!
    @IBOutlet weak var lb_catogery: UILabel!
    @IBOutlet weak var lb_discr: UILabel!
    @IBOutlet weak var lb_Shortdiscr: UILabel!
    @IBOutlet weak var lb_workingTime: UILabel!
    var userProfile : UserStruct?
    var catogerylist = [CategoryStruct]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Profile".localized as NSString, sender: self)

    }
    override func viewWillAppear(_ animated: Bool) {
        userProfile = CurrentUser.userInfo
        lb_name.text = userProfile?.name
        lb_email.text = userProfile?.email
        lb_mobile.text = userProfile?.mobile
        lb_country.text = userProfile?.city?.name
        lb_location.text = userProfile?.address
        lb_discr.text = userProfile?.fullDescription
        lb_Shortdiscr.text = userProfile?.description
        lb_workingTime.text = "\(userProfile?.openingTime ?? "00:00") " + "To".localized + " \(userProfile?.closingTime ?? "00:00")"
        imgProfile.sd_custom(url: (userProfile?.profileImage)!)
        catogerylist = (userProfile?.category?.subCategory)!
        let catoger = catogerylist.map { $0.title as! String }
        lb_catogery.text = catoger.joined(separator: " - ")

    }
    @IBAction func didEditePressed(_ sender: UIButton) {
        let vc:EditeProfileVC = AppDelegate.sb_main.instanceVC()
         vc.userProfile = self.userProfile
        self.navigationController?.pushViewController(vc, animated: true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
