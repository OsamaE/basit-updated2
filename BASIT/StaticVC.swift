//
//  StaticVC.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//
struct StaticObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: ItemsStatic?
}

struct ItemsStatic: Codable {
    let totalProducts, totalOrders, totalNewOrders, totalPrepareOrders: Int?
    let totalOnDeliveryOrders, totalCompletedOrders: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalProducts = "total_products"
        case totalOrders = "total_orders"
        case totalNewOrders = "total_new_orders"
        case totalPrepareOrders = "total_prepare_orders"
        case totalOnDeliveryOrders = "total_onDelivery_orders"
        case totalCompletedOrders = "total_completed_orders"
    }
}


import UIKit

class StaticVC: SuperViewController {
    @IBOutlet weak var pro_orders: UILabel!
    @IBOutlet weak var new_order: UILabel!
    @IBOutlet weak var prep_order: UILabel!
    @IBOutlet weak var delvery_order: UILabel!
    @IBOutlet weak var compl_order: UILabel!
    @IBOutlet weak var total_order: UILabel!
    var StaticInfo : ItemsStatic?

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Statistics".localized as NSString, sender: self)
        loadDate()
        // Do any additional setup after loading the view. getMyStatistics
    }
    
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "getMyStatistics", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StaticObject.self, from: response.data!)
                self.StaticInfo = Status.items!
                self.prep_order.text = "\(self.StaticInfo?.totalPrepareOrders ?? 0)"
                self.pro_orders.text = "\(self.StaticInfo?.totalProducts ?? 0)"
                self.delvery_order.text = "\(self.StaticInfo?.totalOnDeliveryOrders ?? 0)"
                self.total_order.text = "\(self.StaticInfo?.totalOrders ?? 0)"
                self.compl_order.text = "\(self.StaticInfo?.totalCompletedOrders ?? 0)"

//                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
