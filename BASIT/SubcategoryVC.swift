//
//  SubcategoryVC.swift
//  BASIT
//
//  Created by ahmed on 1/22/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class SubcategoryVC: SuperViewController,StoreFilterDelgate,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
   
    
    @IBOutlet weak var collectionSegment: UICollectionView!
    @IBOutlet  var TableView: UITableView!

    var CategoryArray = [CategoryStruct]()
    var StoresArray = [StoreStruct]()

    var Selected_index: Int? = 0

    var Title_cat: String? = ""
    var city_id = 99
    var Mystatus = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionSegment.registerCell(id: "SegmentCell")
        TableView.registerCell(id: "subCategoryCell")
        if !Language.isRtl() {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            //            UserDefaults.standard.set(true, forKey: Language.systemTextDirectionKey)
            //            UserDefaults.standard.set(true, forKey: Language.systemLeftToRightWritingDirectionKey)
            //
            
            UserDefaults.standard.synchronize()
        } else {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            //            UserDefaults.standard.set(false, forKey: Language.systemTextDirectionKey)
            //            UserDefaults.standard.set(false, forKey: Language.systemRightToLeftWritingDirectionKey)
            
            
            UserDefaults.standard.synchronize()
        }
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle( Title_cat! as NSString, sender: self)
        navigation.setRightButtons([navigation.FilterBtn as Any], sender: self)

        if CategoryArray.count > 0 {
        GetData()
        }


    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let vc:StoreFilter = AppDelegate.sb_main.instanceVC()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func SelectedDone(_ status: String, city: Int) {
        city_id = city
        self.Mystatus = status
        GetData()
    }
    func GetData(){
        self.StoresArray.removeAll()
        var link = "getStoresBySubCategory/\(CategoryArray[Selected_index!].isParent!)/\(CategoryArray[Selected_index!].id!)?"
        if city_id != 99{
            link = link + "city=\(city_id )&"
        }
        if self.Mystatus != ""{
            link = link + "status=\(self.Mystatus)"
        }
        _ = WebRequests.setup(controller: self).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StoresObject.self, from: response.data!)
                self.StoresArray = Status.StoreStruct!
                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        self.TableView.reloadData()

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = self.CategoryArray[indexPath.row].title!.width(withConstraintedHeight: 100, font: .systemFont(ofSize: 17)) + 50
        return CGSize(width: w , height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.CategoryArray.count
    }
   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: SegmentCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.title.text = self.CategoryArray[indexPath.row].title?.uppercased()

        if (indexPath.row == self.Selected_index){
            cell.bottome_boarder.isHidden = false

        }else{
            cell.bottome_boarder.isHidden = true
        }
        cell.layoutIfNeeded()

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.Selected_index = indexPath.row
        self.collectionSegment.reloadData()
        GetData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubcategoryVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.StoresArray.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
       return self.StoresArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = StoresArray[indexPath.row]
        let cell: subCategoryCell = tableView.dequeueTVCell()
        cell.lb_title.text = Store.name!
        cell.lb_addres.text = Store.city!.name!
        cell.lb_des.text = Store.description!
        cell.img_thumb.sd_custom(url: Store.profileImage!)
//      let check = Store.status
        if Store.statusNow == "1" {
            cell.lb_stuts.text = "Open".localized
            cell.lb_stuts.textColor = "7AC9C6".color

        }
        else{
            cell.lb_stuts.text = "Close".localized
            cell.lb_stuts.textColor = "D75A4A".color

        }
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
    vc.StoreInfo = StoresArray[indexPath.row]
    self.navigationController?.pushViewController(vc, animated: true)
    }

}
