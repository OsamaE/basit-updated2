//
//  ServicesProviderVC.swift
//  BASIT
//
//  Created by musbah on 3/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ServicesProviderVC: SuperViewController ,ServicesProviderFilterDelgate{
    func SelectedDone(_ jop: Int) {
        city_id = jop
        GetData()

    }
    
    
    
    @IBOutlet weak var tableView: UITableView!

    var data = [ItemServiceProvider]()
    var city_id = 99

    override func viewDidLoad() {
        super.viewDidLoad()
      
    
        
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Services Provider".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setRightButtons([navigation.FilterBtn as Any], sender: self)
        GetData()
        self.tableView.registerCell(id: "ServicesProvidCell")
        
    }
    func GetData(){
        self.data.removeAll()
        self.tableView.reloadData()
        var link = "getServicesProviders?"
        if city_id != 99{
            link = link + "job_id=\(city_id)"
        }
       
        _ = WebRequests.setup(controller: self).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
            do{
                
                
                let object =  try JSONDecoder().decode(ServiceProviderObject.self, from: response.data!)
                self.data = object.items!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let vc:ServicesProviderFilterVC = AppDelegate.sb_main.instanceVC()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
extension ServicesProviderVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.data.count == 0 {
            self.tableView.setEmptyMessage("No Data".localized)
        } else {
            self.tableView.restore()
        }
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = self.data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesProvidCell", for: indexPath) as! ServicesProvidCell
        cell.image_tumb.sd_custom(url: (object.profileImage)!)
        
        cell.lbl_Name.text = object.name
        cell.lbl_Phone.text = object.mobile
        cell.lbl_Email.text = object.email
        cell.lbl_Job.text = object.jobs?.name

        if object.facebook == nil{
           cell.btn_face.isHidden = true
        }else{
            cell.btn_face.isHidden = false
            cell.btn_face.tag = indexPath.row
            cell.btn_face.addTarget(self, action: #selector(pressFaceButton(_:)), for: .touchUpInside)

        }
        if object.twitter == nil{
            cell.btn_twit.isHidden = true

        }else{
            cell.btn_twit.isHidden = false
            cell.btn_twit.tag = indexPath.row
            cell.btn_twit.addTarget(self, action: #selector(pressTwittetButton(_:)), for: .touchUpInside)

        }
        if object.instagram == nil{
            cell.btn_insta.isHidden = true

        }else{
            cell.btn_insta.isHidden = false
            cell.btn_insta.tag = indexPath.row
            cell.btn_insta.addTarget(self, action: #selector(pressInstaButton(_:)), for: .touchUpInside)

        }
        return cell
        
    }
    @objc func pressInstaButton(_ button: UIButton) {
        guard let url = URL(string: self.data[button.tag].instagram!) else { return }
        UIApplication.shared.open(url)
        
    }
    @objc func pressTwittetButton(_ button: UIButton) {
        guard let url = URL(string: self.data[button.tag].twitter!) else { return }
        UIApplication.shared.open(url)
        
    }
    @objc func pressFaceButton(_ button: UIButton) {
        guard let url = URL(string: self.data[button.tag].facebook!) else { return }
        UIApplication.shared.open(url)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 184
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
//extension ServicesProviderVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return data.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServicesProviderCell", for: indexPath) as! ServicesProviderCell
//        let object = self.data[indexPath.row]
//        cell.image.sd_custom(url: (object.profileImage)!)
//
//        cell.lbl_Name.text = object.name
//        cell.lbl_Phone.text = object.mobile
//        cell.lbl_Email.text = object.email
//        cell.lbl_Job.text = object.jobs?.name
//
//
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let screenSize = UIScreen.main.bounds
//        let screenWidth = screenSize.width
//
//        let size = CGSize(width: screenWidth/2, height: 300)
//        return size
//
//        //return CGSize()
//    }
//
//
//
//
//}
