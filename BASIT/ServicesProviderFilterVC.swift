//
//  ServicesProviderFilterVC.swift
//  BASIT
//
//  Created by ahmed on 3/10/19.
//  Copyright © 2019 ahmed. All rights reserved.
//
struct JopObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemJop]?
}

struct ItemJop: Codable {
    let id: Int?
    let name: String?
}



import UIKit
import ActionSheetPicker_3_0

protocol ServicesProviderFilterDelgate : class {
    func SelectedDone(_ jop: Int)
}
class ServicesProviderFilterVC: SuperViewController {
    var CityArray = [ItemJop]()
    @IBOutlet  var lbl_city: UILabel!
    var city_id = 99
    var delegate:ServicesProviderFilterDelgate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Filter".localized as NSString, sender: self)
        _ = WebRequests.setup(controller: self).prepare(query: "getJobs", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(JopObject.self, from: response.data!)
                self.CityArray = Status.items!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: UIButton) {
        delegate?.SelectedDone(city_id)
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Jop".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.lbl_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id!
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
