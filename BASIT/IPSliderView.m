//
//  IPSliderView.m
//  Ajnad
//
//  Created by Haitham on 12/15/14.
//  Copyright (c) 2014 iPhoneAlsham. All rights reserved.
//

#import "IPSliderView.h"

@interface IPSliderView ()

@property (weak, nonatomic) IBOutlet UIImageView *shadawImageView;

@end

@implementation IPSliderView
// #define iPad UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"IPSliderView" owner:self options:nil] firstObject];

    if (self) {
        self.frame = frame;
        
        if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
            self.shadawImageView.layer.transform = CATransform3DMakeRotation(M_PI, 0.0, 1.0, 0.0);
        }
        
   }
    
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


@end
