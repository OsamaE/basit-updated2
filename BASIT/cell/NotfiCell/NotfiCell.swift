//
//  NotfiCell.swift
//  BASIT
//
//  Created by ahmed on 4/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class NotfiCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lbl_data: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
