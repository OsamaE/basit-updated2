//
//  OrderDetailsCell.swift
//  
//
//  Created by musbah on 2/28/19.
//

import UIKit

class OrderDetailsCell: UITableViewCell {

    @IBOutlet weak var OrderImage: UIImageView!
    
    @IBOutlet weak var OrderName: UILabel!
    
    @IBOutlet weak var OrderPrice: UILabel!
    
    @IBOutlet weak var OrderQuntity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
