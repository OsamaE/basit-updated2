//
//  CustomersOrderCell.swift
//  BASIT
//
//  Created by musbah on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class CustomersOrderCell: UITableViewCell {
    
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var Customer: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var TotalPrice: UILabel!
    @IBOutlet weak var orderID: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
