//
//  subCategoryCell.swift
//  BASIT
//
//  Created by ahmed on 1/23/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class subCategoryCell: UITableViewCell {
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_des: UILabel!
    @IBOutlet weak var lb_addres: UILabel!
    @IBOutlet weak var lb_stuts: UILabel!
    @IBOutlet weak var img_thumb: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
