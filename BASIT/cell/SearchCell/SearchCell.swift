//
//  SearchCell.swift
//  BASIT
//
//  Created by ahmed on 3/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Type: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
