//
//  ServicesProvidCell.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ServicesProvidCell: UITableViewCell {
    
    @IBOutlet weak var image_tumb: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var lbl_Email: UILabel!
    
    @IBOutlet weak var lbl_Phone: UILabel!
    
    @IBOutlet weak var lbl_Job: UILabel!
    @IBOutlet weak var btn_face: UIButton!
    @IBOutlet weak var btn_twit: UIButton!
    @IBOutlet weak var btn_insta: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
