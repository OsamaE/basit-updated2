//
//  LoginVC.swift
//  BASIT
//
//  Created by ahmed on 1/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func didSkipButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    @IBAction func didSignInButtonPressed(_ sender: UIButton) {
        
        guard Helper.isConnectedToNetwork() else {
            
//            self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let email = self.tf_email.text, !email.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Email required".localized)
            return
        }
        guard let password = self.tf_password.text, !password.isEmpty else{
            self.showAlert(title:  "Erorr".localized, message: "Password required".localized)
            return
        }
        
        let parameters: [String: Any] = ["email": tf_email.text!,"password":tf_password.text!,"fcm_token":Helper.device_token,"device_type":"ios" ]
 
        _ = WebRequests.setup(controller: self).prepare(query: "login", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status!  {
                    
                    
                    do {
                        let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                        //                    self.userInfo = Status.UserStruct!
                        CurrentUser.userInfo = Status.items
                        print("SessionManager.shared.session", CurrentUser.userInfo as Any)
                        if CurrentUser.userInfo != nil{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
                            self.present(vc, animated: true, completion: nil)
                            
                        }
                        
                    } catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                }else{
                    self.showAlert(title: "Done".localized, message: Status.message!)
                    
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
      

        }
    }
    @IBAction func didForegetButtonPressed(_ sender: UIButton) {
        let vc:ForgetPassword = AppDelegate.sb_main.instanceVC()
        let NavigationController :CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        NavigationController.pushViewController(vc, animated: true)
        self.present(NavigationController, animated: true, completion: nil)

    }
    @IBAction func didSignUpButtonPressed(_ sender: UIButton) {
        let vc:TypeUserVC = AppDelegate.sb_main.instanceVC()
        let NavigationController :CustomNavigationBar = AppDelegate.sb_main.instanceVC()
        NavigationController.pushViewController(vc, animated: true)
        self.present(NavigationController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
