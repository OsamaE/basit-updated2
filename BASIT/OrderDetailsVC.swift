//
//  CustomerOrderVC.swift
//  BASIT
//
//  Created by musbah on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

struct stutsStore: Codable {
    let id: Int?
    let Name: String?

}
class OrderDetailsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var StatusOrder: UILabel!
    
    @IBOutlet weak var AddressOrder: UILabel!
    @IBOutlet weak var lbl_delivery: UILabel!
    @IBOutlet weak var lbl_payment: UILabel!
    @IBOutlet weak var lbl_deliveryCost: UILabel!
    @IBOutlet weak var lbl_deliveryComany: UILabel!

    @IBOutlet weak var view_comany: UIView!

    @IBOutlet weak var OrderDate: UILabel!
    
    @IBOutlet weak var StoreName: UILabel!
    
    @IBOutlet weak var TotalOrder: UILabel!
    
    @IBOutlet weak var OrderID: UILabel!
    @IBOutlet weak var view_action: UIView!

    @IBOutlet weak var changeStatusTF: UITextField!
    @IBOutlet weak var editView: UIView!

    var OrderSend = 0
    var stutSend = 0
    var myOrder = false

    var OrderInfo:OrderItems?
    var stutsArray = [stutsStore]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Order Details".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        loadDate()
        self.tableView.registerCell(id: "OrderDetailsCell")
        stutsArray.append(stutsStore(id: 1, Name: "Preparing".localized))
        stutsArray.append(stutsStore(id: 0, Name: "new".localized))
        stutsArray.append(stutsStore(id: 3, Name: "onDelivery".localized))
        stutsArray.append(stutsStore(id: 4, Name: "Complete".localized))
        tableView.delegate = self
        tableView.dataSource = self
        if myOrder{
            editView.isHidden = true
        }
    }
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "getOrderDetails/\(self.OrderSend)", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(OrderObject.self, from: response.data!)
                self.OrderInfo = Status.items
                self.tableView.reloadData()
                self.showData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }

    func showData(){
        self.OrderID.text = self.OrderInfo?.referenceID
        if self.OrderInfo?.status == "4" {
            view_action.isHidden = true
        }
        self.StatusOrder.text = self.OrderInfo?.changeStatus
        self.AddressOrder.text = self.OrderInfo?.deliveryAddress
        self.OrderDate.text = self.OrderInfo?.deliveryAddress
        self.StoreName.text = self.OrderInfo?.storeName
        
        self.lbl_deliveryCost.text = (self.OrderInfo?.deliveryCost)! + " "  + "QAR".localized
        self.lbl_payment.text = self.OrderInfo?.paymentMethod
        self.lbl_delivery.text = self.OrderInfo?.deliveryMethod
        if self.OrderInfo?.company == nil{
            self.view_comany.isHidden = true
        }else{
            self.lbl_deliveryComany.text = self.OrderInfo?.company?.name
        }
        if self.OrderInfo?.totalPrice != nil {
            self.TotalOrder.text = (self.OrderInfo?.totalPrice)! + " " + "QAR".localized

        }

    }
    @IBAction func changeStatusSelect(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Change Status".localized, rows: self.stutsArray.map { $0.Name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {

                self.changeStatusTF.text = Value as? String
            }
            self.stutSend = self.stutsArray[value].id!
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    
    @IBAction func OkButton(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
            
            //            self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        let parameters: [String: Any] = ["order_id": self.OrderInfo?.id! as Any,"status":self.stutSend ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "changeOrderStatus", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    self.navigationController?.popViewController(animated: true)

                }else{
                        self.showAlert(title: "Error".localized, message: Status.message!)
                        
                
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
}






extension OrderDetailsVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.OrderInfo == nil {
            return 0
        }
        if self.OrderInfo?.products?.count == 0 {
            self.tableView.setEmptyMessage("No Data".localized)
        } else {
            self.tableView.restore()
        }
        return (self.OrderInfo?.products?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderProduct = self.OrderInfo?.products![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell", for: indexPath) as! OrderDetailsCell
        cell.OrderName.text = orderProduct?.product?.name
        cell.OrderQuntity.text = "QTR".localized + " " + (orderProduct?.quantity)!
        cell.OrderPrice.text = (orderProduct?.product?.price)! + " " + "QAR".localized
        cell.OrderImage.sd_custom(url: (orderProduct!.product?.image)!)

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
        //        vc.StoreInfo = StoresArray[indexPath.row]
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
