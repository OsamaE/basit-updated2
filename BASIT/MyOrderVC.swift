//
//  MyOrderVC.swift
//  BASIT
//
//  Created by ahmed on 3/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class MyOrderVC: SuperViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet  var TableView: UITableView!
    @IBOutlet weak var collectionSegment: UICollectionView!

    var status = 0
    var OrderArray = [ItemCustomerOrder]()
    var CategoryArray = [String]()
    var Selected_index: Int? = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle( ("My Order".localized as? NSString)!, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        self.TableView.registerCell(id: "CustomersOrderCell")
        collectionSegment.registerCell(id: "SegmentCell")

        CategoryArray = ["NEW","PREPARING","READY","ON DELIVERY","COMPLETE"]
        collectionSegment.reloadData()

        loadDate()

        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = self.CategoryArray[indexPath.row].width(withConstraintedHeight: 100, font: .systemFont(ofSize: 17)) + 30
        return CGSize(width: w , height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.CategoryArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: SegmentCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.title.text = self.CategoryArray[indexPath.row].localized
        
        if (indexPath.row == self.Selected_index){
            cell.bottome_boarder.isHidden = false
            
        }else{
            cell.bottome_boarder.isHidden = true
        }
        cell.layoutIfNeeded()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.Selected_index = indexPath.row
        status = indexPath.row
        self.collectionSegment.reloadData()
        loadDate()
        
    }

  
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "getMyOrders?status=\(status)", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(customerOrderObject.self, from: response.data!)
                self.OrderArray = Status.items!
                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyOrderVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if OrderArray.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
        return self.OrderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = self.OrderArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersOrderCell", for: indexPath) as! CustomersOrderCell
        cell.orderDate.text = order.createdAt?.substring(0..<11)
        
        cell.Customer.text = order.customerName
        cell.Status.text = order.changeStatus
        cell.TotalPrice.text = order.totalPrice
        cell.orderID.text = "Order".localized + " \(order.id!)"

        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 222
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:OrderDetailsVC = AppDelegate.sb_main.instanceVC()
        vc.OrderSend = OrderArray[indexPath.row].id!

        vc.myOrder = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
public extension String {
    
    //right is the first encountered string after left
    func between(_ left: String, _ right: String) -> String? {
        guard
            let leftRange = range(of: left), let rightRange = range(of: right, options: .backwards)
            , leftRange.upperBound <= rightRange.lowerBound
            else { return nil }
        
        let sub = self[leftRange.upperBound...]
        let closestToLeftRange = sub.range(of: right)!
        return String(sub[..<closestToLeftRange.lowerBound])
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    func character(_ at: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: at)]
    }
    
    func lastIndexOfCharacter(_ c: Character) -> Int? {
        return range(of: String(c), options: .backwards)?.lowerBound.encodedOffset
    }
}
