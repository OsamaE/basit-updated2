//
//  SignupVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

protocol SubCategoryDelgate : class {
     func SelectedDone(_ sender: [CategoryStruct])
}

class SignUpSubCategory: SuperViewController,UITableViewDelegate,UITableViewDataSource{
    
   
    var delegate:SubCategoryDelgate?
    var obj : [CategoryStruct] = []
    var selected : [CategoryStruct] = []
    //var Obj:[CategoryStruct]?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Sub Category".localized as NSString, sender: self)
        navigation.setRightButtons([navigation.setBtnTitle(title: "Done".localized) as Any], sender: self)

    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        delegate?.SelectedDone(self.selected)
        self.navigationController?.popViewController(animated: true)
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return obj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = obj[indexPath.row].title
        
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell =  tableView.cellForRow(at: indexPath)
        if  cell?.accessoryType != .checkmark {
            cell?.accessoryType = .checkmark
            self.selected.append(obj[indexPath.row])
            print(selected)        }else{
            cell?.accessoryType = .none
            selected  = selected.filter { $0.id != obj[indexPath.row].id }
            print(selected)
        }
    }
}
