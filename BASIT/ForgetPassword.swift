//
//  ForgetPassword.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ForgetPassword: SuperViewController {
    @IBOutlet weak var tf_email: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonWithdismiss(sender: self)
        navigation.setTitle("Forget Password".localized as NSString, sender: self)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func Send(_ sender: UIButton) {
        let parameters: [String: Any] = ["email":self.tf_email.text! ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "forgetpassword", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    self.showAlert(title: "Success".localized, message: Status.message!)
                    //                    self.navigationController?.popViewController(animated: true)
                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
