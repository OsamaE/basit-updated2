//
//  ContactUsVC.swift
//  BASIT
//
//  Created by ahmed on 3/1/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import GoogleMaps
import MessageUI
import Alamofire
class ContactUsVC: SuperViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var tf_email: UITextField!
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var tf_massage: UITextView!
    @IBOutlet weak var mapView: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_email.text = (CurrentUser.Setting?.infoEmail)!
        lbl_mobile.text = (CurrentUser.Setting?.mobile)!
        lbl_phone.text = (CurrentUser.Setting?.phone)!
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("ContactUS".localized as NSString, sender: self)
        
        guard let amount = CurrentUser.Setting?.latitude else { return }
        guard let amount2 = CurrentUser.Setting?.longitude  else { return }
        
        let lat = Double(amount)
        let lang = Double(amount2)
        
        let coordinates = CLLocationCoordinate2DMake(lat!, lang!)
        let marker = GMSMarker(position: coordinates)
        marker.map = self.mapView
        self.mapView.animate(toLocation: coordinates)
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat!, longitude: lang!, zoom: 8.0)
        self.mapView.camera = camera
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func openFace(_ sender: UIButton) {
        guard let url = URL(string: (CurrentUser.Setting?.facebook)!) else { return }
        UIApplication.shared.open(url)

    }
    @IBAction func CallMobile(_ sender: UIButton) {
        if let url = NSURL(string: "tel://\(CurrentUser.Setting?.mobile ?? "0")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }

    }
    @IBAction func CallPhone(_ sender: UIButton) {
        if let url = NSURL(string: "tel://\(CurrentUser.Setting?.phone ?? "0" )"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    @IBAction func CallEmail(_ sender: UIButton) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([(CurrentUser.Setting?.adminEmail)!])
        composeVC.setSubject("Message Subject")
        composeVC.setMessageBody("Message content.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)

    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func openinsta(_ sender: UIButton) {
        guard let url = URL(string: (CurrentUser.Setting?.instagram)!) else { return }
        UIApplication.shared.open(url)

    }
    
    @IBAction func openTwot(_ sender: UIButton) {
        guard let url = URL(string: (CurrentUser.Setting?.twitter)!) else { return }
        UIApplication.shared.open(url)

    }
    
    @IBAction func Send(_ sender: UIButton) {
        
   
        

        let parameters: [String: Any] = ["name": self.tf_name.text! ,"email":self.tf_email.text! ,"message": self.tf_massage.text!]

        _ = WebRequests.setup(controller: self).prepare(query: "contact", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
//                    self.showAlert(title: "Success".localized, message: Status.message!)
                    //                    self.navigationController?.popViewController(animated: true)
                    self.navigationController?.popViewController(animated: true)

                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)


                }

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }

        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
