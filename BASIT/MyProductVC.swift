//
//  MyOrderVC.swift
//  BASIT
//
//  Created by ahmed on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class MyProductVC: SuperViewController {
    var ProductsArray = [ProductStruct]()
    @IBOutlet  var TableView: UITableView!
    @IBOutlet  var btn_curent: UIButton!
    @IBOutlet  var btn_complete: UIButton!
    var status = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TableView.registerCell(id: "MyProdCell")
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle( ("My Product".localized as? NSString)!, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setRightButtons([navigation.setBtnTitle(title: "+".localized) as Any], sender: self)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        loadDate()

    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let vc:NewOrderVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func ChangeStatus(_ sender: UIButton) {
        if sender.tag == 0{
            self.btn_curent.addBottomBorderWithColor(color: "649AB8".color, width: 1)
            self.btn_complete.addBottomBorderWithColor(color: "649AB8".color, width: 0)

            loadDate()
        }else{
            self.btn_complete.addBottomBorderWithColor(color: "649AB8".color, width: 1)
            self.btn_curent.addBottomBorderWithColor(color: "649AB8".color, width: 0)

            loadDate()

        }
        status = sender.tag
    }
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "getMyProducts", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(ProductsObject.self, from: response.data!)
                self.ProductsArray = Status.ProductStruct!
                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MyProductVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProductsArray.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
        return (ProductsArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = ProductsArray[indexPath.row]
        let cell: MyProdCell = tableView.dequeueTVCell()
        cell.lb_title.text = Store.name!
        cell.lb_price.text = Store.price! + " " + "QAR".localized
        cell.img_thumb.sd_custom(url: (Store.image)!)
        
                cell.btn_edit.tag = indexPath.row
                cell.btn_edit.addTarget(self, action: #selector(pressEditButton(_:)), for: .touchUpInside)

        cell.btn_remove.tag = indexPath.row
        cell.btn_remove.addTarget(self, action: #selector(pressDeletButton(_:)), for: .touchUpInside)
        cell.btn_qun.tag = Store.id!
        cell.btn_qun.addTarget(self, action: #selector(pressqunButton(_:)), for: .touchUpInside)

        return cell
        
    }
    @objc func pressqunButton(_ button: UIButton) {
        let vc:AddQuntity = AppDelegate.sb_main.instanceVC()
        vc.id = button.tag
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        
        present(vc, animated: true, completion: nil)
    }
    @objc func pressDeletButton(_ button: UIButton) {
        
        let controller = UIAlertController(title:"", message: "Are you sure you want to delete?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
            _ = WebRequests.setup(controller: self).prepare(query: "deleteProduct/"+"\(self.ProductsArray[button.tag].id ?? 0)", method: HTTPMethod.delete, isAuthRequired : true).start(){ (response, error) in
                            self.loadDate()
                        }
            
        
        })
        let no = UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action) in
        })
        controller.addAction(yes)
        controller.addAction(no)
        self.present(controller, animated: true, completion: nil)
    }
    @objc func pressEditButton(_ button: UIButton) {
        let vc:NewOrderVC = AppDelegate.sb_main.instanceVC()
        vc.ProductInfo = ProductsArray[button.tag]
        vc.IsEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
        vc.ProductInfo = ProductsArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didcheckoutPressed(_ sender: UIButton) {
        let vc:checkout = AppDelegate.sb_main.instanceVC()
        //        vc.cartItem = self.cartItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
