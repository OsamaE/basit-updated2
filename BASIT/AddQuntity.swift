//
//  AddQuntity.swift
//  BASIT
//
//  Created by ahmed on 4/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class AddQuntity: UIViewController {
    @IBOutlet  var lbl_qun: UITextField!
    var id = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        
        guard let qun = self.lbl_qun.text , !qun.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Quantity required".localized)
            return
        }
        let parameters: [String: Any] = ["product_id": self.id ,"quantity" : qun]
        
        _ = WebRequests.setup(controller: self).prepare(query: "addQuantity", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    
                    self.dismiss(animated: true, completion: nil)

                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }

    }
    
    @IBAction func CloseAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
