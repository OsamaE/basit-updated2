//
//  CustomerProvileVC.swift
//  BASIT
//
//  Created by musbah on 2/27/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import GoogleMaps

class ServicesProvileVC: UIViewController {
    
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var lb_mobile: UILabel!
    @IBOutlet weak var lb_city: UILabel!
    @IBOutlet weak var lb_location: UILabel!
    
    @IBOutlet weak var lbl_Job: UILabel!
    
    
    var userProfile : UserStruct?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Profile".localized as NSString, sender: self)
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        userProfile = CurrentUser.userInfo
        
        imgProfile.sd_custom(url: (userProfile?.profileImage)!)
        lb_name.text = userProfile?.name
        lb_email.text = userProfile?.email
        lb_mobile.text = userProfile?.mobile
        lb_city.text = userProfile?.city?.name
        lb_location.text = userProfile?.address
        lbl_Job.text = userProfile?.jobs?.name

    }
    
    @IBAction func didEditePressed(_ sender: UIButton) {
        let vc:EditeServicesProfileVC = AppDelegate.sb_main.instanceVC()
        vc.userProfile = self.userProfile
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
