//
//  CustomerOrderVC.swift
//  BASIT
//
//  Created by musbah on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class CustomerOrderVC: SuperViewController ,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionSegment: UICollectionView!
    var CategoryArray = [String]()
    var Selected_index: Int? = 0

    var OrderArray = [ItemCustomerOrder]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Customer Order".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        self.tableView.registerCell(id: "CustomersOrderCell")
        collectionSegment.registerCell(id: "SegmentCell")
        CategoryArray = ["NEW","PREPARING","READY","ON DELIVERY","COMPLETE"]
        collectionSegment.reloadData()
        tableView.delegate = self
        tableView.dataSource = self

     
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = self.CategoryArray[indexPath.row].width(withConstraintedHeight: 100, font: .systemFont(ofSize: 17)) + 30
        return CGSize(width: w , height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.CategoryArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: SegmentCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.title.text = self.CategoryArray[indexPath.row].localized
        
        if (indexPath.row == self.Selected_index){
            cell.bottome_boarder.isHidden = false
            
        }else{
            cell.bottome_boarder.isHidden = true
        }
        cell.layoutIfNeeded()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.Selected_index = indexPath.row
        self.collectionSegment.reloadData()
        GetData()
    }
    func GetData(){
        _ = WebRequests.setup(controller: self).prepare(query: "customersOrders" + "/\(self.Selected_index ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do{
                
                
                let object =  try JSONDecoder().decode(customerOrderObject.self, from: response.data!)
                self.OrderArray = object.items!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        GetData()
    }
}





extension CustomerOrderVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if OrderArray.count == 0 {
            self.tableView.setEmptyMessage("No Data".localized)
        } else {
            self.tableView.restore()
        }
        return self.OrderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = self.OrderArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomersOrderCell", for: indexPath) as! CustomersOrderCell
        let date = String((order.createdAt?.prefix(11))!) // Hell
        cell.orderID.text = "Order".localized + " \(order.id!)"
        cell.orderDate.text = date
        cell.Customer.text = order.customerName
        cell.Status.text = order.changeStatus
        cell.TotalPrice.text = order.totalPrice ?? "0" + " " + "QAR".localized


       return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 222
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:OrderDetailsVC = AppDelegate.sb_main.instanceVC()
        
        vc.OrderSend = OrderArray[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
