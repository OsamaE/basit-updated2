//
//  ChangePassword.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ChangePassword: SuperViewController {
    @IBOutlet weak var tf_old: UITextField!
    @IBOutlet weak var tf_password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Change Password".localized as NSString, sender: self)

        // Do any additional setup after loading the view.
    }
    @IBAction func Send(_ sender: UIButton) {
        let parameters: [String: Any] = ["old_password": self.tf_old.text! ,"password":self.tf_password.text! ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "changePassword", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    self.showAlert(title: "Success".localized, message: Status.message!)
                    //                    self.navigationController?.popViewController(animated: true)
                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
