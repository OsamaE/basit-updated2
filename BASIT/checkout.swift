//
//  checkout.swift
//  BASIT
//
//  Created by ahmed on 2/27/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class checkout: SuperViewController {
    @IBOutlet  var TableView: UITableView!
    @IBOutlet  var lbl_city: UILabel!
    @IBOutlet  var lbl_total: UILabel!
    @IBOutlet  var lbl_deliveryCost: UILabel!
    @IBOutlet  var lbl_company: UILabel!
    @IBOutlet  var tf_mobile: UITextField!
    @IBOutlet  var tv_address: UITextView!
    @IBOutlet weak var btn_Delivery: SelectButton!
    @IBOutlet weak var btn_Pickup: SelectButton!
    @IBOutlet  var view_Delivery: UIView!
    @IBOutlet weak var btn_cash: SelectButton!
    @IBOutlet weak var btn_onlie: SelectButton!

    var CityArray = [CityStruct]()
    var comniesArray = [campanyItem]()
    var city_id :Int!
    var compny_id :Int!
    var payment_method = "cash"
    var delivery_method = ""
    var delivery_address = ""
    var delivery_company_id = 99
    var cartItem : cartItems?
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Checkout".localized as NSString, sender: self)

        self.TableView.registerCell(id: "cartCell")
        view_Delivery.isHidden = true
        btn_Delivery?.alternateButton = [btn_Pickup!]
        btn_Pickup?.alternateButton = [btn_Delivery!]
        tf_mobile.text =  CurrentUser.userInfo?.mobile
        tv_address.text =  CurrentUser.userInfo?.address
        if CurrentUser.userInfo?.city != nil{
        lbl_city.text =  CurrentUser.userInfo?.city?.name
        }
        city_id = CurrentUser.userInfo?.city?.id
        btn_cash?.alternateButton = [btn_onlie!]
        btn_onlie?.alternateButton = [btn_cash!]

        btn_Delivery.isSelected = false
        btn_Pickup.isSelected = false
        btn_onlie.isSelected = false
        btn_cash.isSelected = false

        self.lbl_total.text = "\(self.cartItem?.totalPrice ?? 0.0) " + "QAR".localized

        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "deliveryCompanies", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(campanyObject.self, from: response.data!)
                self.comniesArray = Status.items!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
    }
    
    @IBAction func OkButton(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
            
            //            self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        if city_id ==  nil  {
            self.showAlert(title: "Error".localized, message: "City Required".localized)
return
            
        }
        if self.delivery_method ==  ""  {
            self.showAlert(title: "Error".localized, message: "Delivery method Required".localized)
            return
            
        }

        var parameters: [String: Any] = ["payment_method": self.payment_method ,"cityTo":city_id!,"delivery_address":self.tv_address.text! ,"delivery_method": self.delivery_method]
        if compny_id !=  nil{
            parameters["delivery_company_id"] = self.compny_id
        }
        _ = WebRequests.setup(controller: self).prepare(query: "checkOut", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    if self.payment_method == "online" {
                    do {
                        let Status =  try JSONDecoder().decode(checkoutStruct.self, from: response.data!)
                        //                    self.userInfo = Status.UserStruct
                        guard let url = URL(string: Status.url!) else { return }
                        UIApplication.shared.open(url)

                    } catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                        }
                        self.tabBarController?.selectedIndex = 2
                        
                        self.navigationController?.popViewController(animated: true)

                    }
                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    func getDelivery() {
        if compny_id ==  nil || city_id ==  nil {
            return
        }
        let parameters: [String: Any] = ["delivery_company_id":compny_id!,"cityTo":city_id! ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "costDelivery", method: HTTPMethod.post, parameters: parameters, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(costDeliveryObject.self, from: response.data!)
                if Status.costDelivery != nil{
                    self.view_Delivery.isHidden = false
                    self.lbl_deliveryCost.text = Status.costDelivery! + " " + "QAR".localized
                        
                    if let cost = Double(Status.costDelivery!) {
                        self.lbl_total.text = "\((self.cartItem?.totalPrice)! + cost ) " + "QAR".localized

                    }
                   
                }else{
                    self.lbl_total.text = "\(self.cartItem?.totalPrice ?? 0.0) " + "QAR".localized

                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func selectPaymentMethod(_ sender: UIButton) {
        if sender.tag == 0{
            self.payment_method = "cash"
            
            //            self.view_Delivery.isHidden = false
        }else{
            self.payment_method = "online"
            
            //            self.view_Delivery.isHidden = true
        }
        
    }
    
    @IBAction func selectDeliveyMethod(_ sender: UIButton) {
       
        if sender.tag == 0{
            self.delivery_method = "delivery"
            
            //            self.view_Delivery.isHidden = false
        }else{
            self.delivery_method = "pickup"
            
            //            self.view_Delivery.isHidden = true
        }
    }
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.lbl_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id
            self.getDelivery()
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func selectCompany(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Select Campany".localized, rows: self.comniesArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.lbl_company.text = Value as? String
            }
            self.compny_id = self.comniesArray[value].id
            self.getDelivery()
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    

}
extension checkout: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cartItem.self == nil{
            return 0
        }
        
        if cartItem?.cart!.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
        return (cartItem?.cart?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = cartItem?.cart![indexPath.row]
        let cell: cartCell = tableView.dequeueTVCell()
        cell.lb_catogry.text = Store?.storeName!
        cell.lb_title.text = Store?.product?.name ?? ""
//        cell.lb_price.text = Store?.product?.price!
        if Store?.product?.priceAfterOffer != nil {
            cell.lb_price.text = (Store?.product?.priceAfterOffer!)! + " " + "QAR".localized
        }else{
            cell.lb_price.text = (Store?.product?.price!)! + " " + "QAR".localized
        }
        cell.img_thumb.sd_custom(url: (Store?.product?.image!)!)
        cell.btn_add.isHidden = true
        cell.btn_remove.isHidden = true
        cell.btn_delet.isHidden = true
        cell.lb_qun.text = Store?.quantity!

        return cell
        
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
        //        vc.StoreInfo = StoresArray[indexPath.row]
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
class SelectButton: UIButton {
    var alternateButton:Array<SelectButton>?
    let checkedImage = UIImage(named: "ic_check_box")! as UIImage
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
    
    override func awakeFromNib() {
        //        self.layer.cornerRadius = 5
        //        self.layer.borderWidth = 2.0
        //        self.layer.masksToBounds = true
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:SelectButton in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
}
