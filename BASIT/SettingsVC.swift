//
//  ViewController.swift
//  BASIT
//
//  Created by ahmed on 1/16/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class SettingsVC: SuperViewController {

    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var vi_storStuts: UIView!
    @IBOutlet weak var vi_restPass: UIView!
    @IBOutlet weak var switch_stuts: UISwitch!
    @IBOutlet weak var vi_ChangPass: UIView!

    @IBOutlet weak var vi_Logout: UIView!
    var flipOption : UIView.AnimationOptions = .transitionFlipFromLeft

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Settings".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        
        if CurrentUser.userInfo == nil{
            self.vi_storStuts.isHidden = true
            self.vi_Logout.isHidden = true
            self.vi_restPass.isHidden = true
            self.vi_ChangPass.isHidden = true

        }else{
            if CurrentUser.userInfo?.type == "0" {
                self.vi_storStuts.isHidden = true

            }
            if CurrentUser.userInfo?.type == "1" { //store
                self.vi_storStuts.isHidden = false
                if CurrentUser.userInfo?.statusNow == "1"{
                    self.switch_stuts.isOn = true
                    
                }else{
                    self.switch_stuts.isOn = false

                }
                
            }
            if CurrentUser.userInfo?.type == "2" { //prov
                self.vi_storStuts.isHidden = true

            }
            self.viewWillLayoutSubviews()
        }
    }
    @IBAction func changeStatusStore(_ sender: Any) {
        
        var parameters: [String: Any] = [:]
        if self.switch_stuts.isOn {
            parameters ["status"] = "0"
        }else{
            parameters ["status"] = "1"

        }
            
        _ = WebRequests.setup(controller: self).prepare(query: "changeStatusStore", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{
                    do {
                        let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                        CurrentUser.userInfo = Status.items
//                        self.navigationController?.popViewController(animated: true)
                        
                    } catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
//                    self.self.switch_stuts.setOn(!self.switch_stuts.isOn, animated: true)
                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }

    @IBAction func NotificationsSwitch(_ sender: Any) {
    }
    @IBAction func changeLanguageAction(_ sender: Any) {
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let en_action = UIAlertAction.init(title: "English", style: .default) { (_) in
//            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//            MOLH.reset(transition: .transitionCrossDissolve)
            MOLH.setLanguageTo("en")
            MOLH.reset(transition: .transitionCrossDissolve)

            self.didLanguageChanged(key: "en", flip: .transitionFlipFromLeft, sem: .forceLeftToRight)
        }
        
        let ar_action = UIAlertAction.init(title: "العربية", style: .default) { (_) in
//            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "ar" ? "en" : "ar")
//            MOLH.reset(transition: .transitionCrossDissolve)
//
            MOLH.setLanguageTo("ar")
            MOLH.reset(transition: .transitionCrossDissolve)

//            self.didLanguageChanged(key: "ar", flip: .transitionFlipFromRight, sem: .forceRightToLeft)
        }
        let cancel = UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil)
        
        alert.addAction(ar_action)
        alert.addAction(en_action)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)

    }
    private func didLanguageChanged(key: String, flip: UIView.AnimationOptions, sem: UISemanticContentAttribute){
        Language.setAppLanguage(lang: key)
        UIView.appearance().semanticContentAttribute = sem
        self.flipOption = flip
        
//        goHome()
        
        //guard let window = UIApplication.shared.keyWindow else { return }
        //let vc: RootViewController = AppDelegate.sb_main.instanceVC()
        //window.rootViewController = vc
        //window.makeKeyAndVisible()
        //UIView.transition(with: window, duration: 0.5, options: self.flipOption, animations: nil, completion: nil)
    }
    func goHome(){
        guard let window = UIApplication.shared.keyWindow else { return }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
        window.rootViewController = vc
        window.makeKeyAndVisible()
        
        UIView.transition(with: window, duration: 0.5, options: self.flipOption, animations: nil, completion: nil)
        view.endEditing(true)
    }
    @IBAction func changePasswordAction(_ sender: Any) {
        let vc:ChangePassword = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    @IBAction func StoreStatusSwitch(_ sender: Any) {
    }
    
    @IBAction func TremsOfUseAction(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.Info = CurrentUser.Setting?.terms
        vc.IsImage = false
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func PrivacyPolicyAction(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.Info = CurrentUser.Setting?.privacy
        vc.IsImage = false
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func SignOutAction(_ sender: Any) {
        let controller = UIAlertController(title:"", message: "Are you sure to SignOut?".localized, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
            CurrentUser.userInfo = nil
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            self.present(vc , animated: true)
            })
        
        let no = UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action) in
            })
        controller.addAction(yes)
        controller.addAction(no)
        self.present(controller, animated: true, completion: nil)
    }
    
}

