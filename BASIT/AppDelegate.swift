//
//  AppDelegate.swift
//  BASIT
//
//  Created by ahmed on 1/16/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import BRYXBanner
import Firebase
import FirebaseMessaging
import UserNotifications


struct CheckStatusStruct : Codable{
    let status: Bool?
    let code,logout: Int?
    let message: String?
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MOLHResetable {

    var window: UIWindow?
    let gcmMessageIDKey         = "gcm.message_id"

    static let sb_main = UIStoryboard.init(name: "Main", bundle: nil)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = .white
        
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false

        IQKeyboardManager.shared.toolbarBarTintColor = "649AB8".color
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
        GMSServices.provideAPIKey("AIzaSyByvjlv8TxC1GjOuI0IfwRExDcpj1ACCJE")
        GMSPlacesClient.provideAPIKey("AIzaSyByvjlv8TxC1GjOuI0IfwRExDcpj1ACCJE")
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in
        })
        application.registerForRemoteNotifications()
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        Messaging.messaging().subscribe(toTopic: "basit")
        
 connectToFcm()
//        Localizer.DoTheExchange()
        
//        MOLHLanguagesetDefaultLanguage("ar")
        MOLH.shared.activate(true)

        _ = WebRequests.setup(controller: nil).prepare(query: "checkStatus", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CheckStatusStruct.self, from: response.data!)
                if Status.logout == 1{
                    CurrentUser.userInfo = nil

                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        // Override point for customization after application launch.
        return true
    }
    func reset() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController

        
        
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        let stry = UIStoryboard(name: "Main", bundle: nil)
//        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "rootnav")
    }
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                let refreshedToken = InstanceID.instanceID().token()
                if refreshedToken != nil{
                    print("InstanceID token12: \(String(describing: refreshedToken))")
                    UserDefaults.standard.set(refreshedToken, forKey: "token") //setObject
                Helper.device_token = refreshedToken ?? ""
                }else{
                    print("Connected to FCM refreshedToken nill.")

                }
                Messaging.messaging().subscribe(toTopic: "basit")
                
            }
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
        if let refreshedToken = InstanceID.instanceID().token()
        {
//            Helper.device_token = refreshedToken
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // print(userInfo)
        // print full message.
        if let aps = userInfo["aps"] as? Dictionary<String, AnyObject> {
            //            let alert = aps["alert"] as? NSDictionary
            
            print(aps)
            
            let state = UIApplication.shared.applicationState
            
            if state == .active{
                
                print("app active")
                
                if let aps = userInfo["aps"] as? Dictionary<String, AnyObject> {
                    let typetype = userInfo["type"] as? Int ?? 0
                    print(typetype)
                    //let uid = userInfo["gcm.notification.uid"] as! String
                    
                    if let alert = aps["alert"] as? NSDictionary{
                        print("Received Push Badge: \(alert)")
                        
                        let body = alert.value(forKey: "body") as? String ?? ""
                        let title = alert.value(forKey: "title") as! String
                        
                        let banner = Banner(title: title, subtitle: body, image: nil, backgroundColor: "000080".color)
                        banner.dismissesOnTap = true
                        banner.show(duration: 3.0)
                    }
                }
            }else{
                
                print("Fooo")
                //let type = userInfo["gcm.notification.type"] as? String ?? ""
                //let id = userInfo["id"] as? String ?? ""
            }
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
}
extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        //save FCM token to UserDefaults:
//        Helper.device_token = fcmToken
        
        print(fcmToken)
        
        guard let token = InstanceID.instanceID().token() else { return }
        print(token)
        
        let _token = Messaging.messaging().fcmToken
        print("FCM token: \(_token ?? "")")
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
