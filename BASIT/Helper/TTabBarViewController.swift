//
//  TTabBarViewController.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class TTabBarViewController: UITabBarController {
    var leftColor = "74BCC2".color
    var rightColor = "649AB8".color
    let layerGradient = CAGradientLayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        layerGradient.colors = [leftColor.cgColor, rightColor.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.tabBar.layer.insertSublayer(layerGradient, at:0)
        if CurrentUser.userInfo == nil{
            self.viewControllers?.remove(at: 1)
            self.viewControllers?.remove(at: 2)

        }else{
            if CurrentUser.userInfo?.type == "0" {
                self.viewControllers?.remove(at: 1)
                self.viewControllers?.remove(at: 2)
            }
            if CurrentUser.userInfo?.type == "1" { //store
                self.viewControllers?.remove(at: 2)
                self.viewControllers?.remove(at: 2)
            }
            if CurrentUser.userInfo?.type == "2" {
                self.viewControllers?.remove(at: 1)
                self.viewControllers?.remove(at: 1)
            }
            self.viewWillLayoutSubviews()
        }
     
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let items = self.tabBar.items{
            for item in items{
                item.selectedImage =  item.image?.withRenderingMode(.alwaysOriginal)
//                item.title = ""
//                self.tabBar.selectedItem?.title = ""
            }
        }

        self.tabBar.unselectedItemTintColor = "B6D6DE".color
        
        
        
        
        //        tabItem.selectedImage = tabItem.image?.withRenderingMode(.alwaysOriginal)
        
        //        // Draw Indicator above the tab bar items
        //        guard let numberOfTabs = tabBar.items?.count else {
        //            return
        //        }
        //
        //        let numberOfTabsFloat = CGFloat(numberOfTabs)
        //        let imageSize = CGSize(width: 20,
        //                               height: tabBar.frame.height)
        //
        //
        //        let indicatorImage = UIImage.drawTabBarIndicator(color: .white,
        //                                                         size: imageSize,
        //                                                         onTop: onTopIndicator)
        //     //   self.tabBar.selectionIndicatorImage = indicatorImage
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
