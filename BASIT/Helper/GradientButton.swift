//
//  GradientButton.swift
//  BASIT
//
//  Created by ahmed on 1/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class GradientButton : UIButton {
    
    var leftColor = "5D8AB4".color
    var rightColor = "7AC9C6".color
    public let buttongradient: CAGradientLayer = CAGradientLayer()
    
    override var isSelected: Bool {  // or isHighlighted?
        didSet {
            updateGradientColors()
        }
    }
    
    func updateGradientColors() {
        let colors: [UIColor]
        
        if isSelected {
            colors = [leftColor, rightColor]
        } else {
            colors = [leftColor, rightColor]
        }
        
        buttongradient.colors = colors.map { $0.cgColor }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupGradient()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateGradient()
    }
    
    func setupGradient() {
        buttongradient.startPoint = CGPoint.init(x: 0, y: 0.5)
        buttongradient.endPoint = CGPoint.init(x: 1, y: 0.5)
        self.layer.insertSublayer(buttongradient, at: 0)
        updateGradientColors()
    }
    
    func updateGradient() {
        buttongradient.frame = self.bounds
//        buttongradient.cornerRadius = buttongradient.frame.height / 2
    }
    
}



