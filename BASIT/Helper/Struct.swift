//
//  Struct.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import Foundation
struct CurrentUser  {
    
    static var userInfo : UserStruct?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentUser");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentUser")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentUser") as? Data {
                return try? PropertyListDecoder().decode(UserStruct.self, from:data)
                
            }
            return nil
        }
        
    }
    

    
    static var Setting : ItemsSetting?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentSetting");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentSetting")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentSetting") as? Data {
                return try? PropertyListDecoder().decode(ItemsSetting.self, from:data)
                
            }
            return nil
        }
        
    }
    
    
}
struct UserObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: UserStruct?
}

struct UserStruct: Codable {
    let id: Int?
    let name, email: String?
    let profileImage: String?
    let mobile, status,statusNow, type, cityID: String?
    let lat, lng, address, description: String?
    let fullDescription, openingTime, closingTime, categoryID: String?
    let facebook, twitter, instagram, deliveryCost: String?
    let jobID: String?
    let close, accessToken: String?
    let notification: [Notification]?
    let category: CategoryStruct?
    let city: CityStruct?
    let jobs: CityStruct?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case profileImage = "profile_image"
        case mobile, status, type
        case cityID = "city_id"
        case statusNow = "status_now"

        case lat, lng, address, description
        case fullDescription = "full_description"
        case openingTime = "opening_time"
        case closingTime = "closing_time"
        case categoryID = "category_id"
        case facebook, twitter
        case instagram = "instagram"
        case deliveryCost = "delivery_cost"
        case jobID = "job_id"
        case close
        case accessToken = "access_token"
        case notification, category, city, jobs
    }
}

struct CategoriesObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let Categories: [CategoryStruct]?
    
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case Categories = "items"
    }
}

struct CategoryStruct: Codable {
    let id: Int?
    let logo: String?
    let isParent: String?
    let subCategory: [CategoryStruct]?
    let title: String?
    
    enum CodingKeys: String, CodingKey {
        case id, logo
        case isParent = "is_parent"
        case subCategory = "sub_category"
        case title
    }
}


struct CityObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let CityStruct: [CityStruct]?
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case CityStruct = "items"
    }
}

struct JobsObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemJob]?
}

struct ItemJob: Codable {
    let id: Int?
    let name: String?
    
}
struct CityStruct: Codable {
    let id: Int?
    let parent: String?
    let name: String?
//    let translations: [Translation]?
}

struct Translation: Codable {
    let id: Int?
    let cityID, name, locale, updatedAt: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case cityID = "city_id"
        case name, locale
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}



struct campanyObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [campanyItem]?
}
struct costDeliveryObject: Codable {
    let status: Bool?
    let code: Int?
    let message, costDelivery: String?
}
struct campanyItem: Codable {
    let id: Int?
    let mobile, email: String?
    let logo: String?
    let status: String?
    let url: String?
    let updatedAt, createdAt, deletedAt: String?
    let name: String?
    let address: String?
    
    enum CodingKeys: String, CodingKey {
        case id, mobile, email, logo, status, url
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case deletedAt = "deleted_at"
        case name, address
    }
}
struct StoresObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let StoreStruct: [StoreStruct]?
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case StoreStruct = "items"
    }
}
struct StoresInfoObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let StoreStruct: StoreStruct?
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case StoreStruct = "items"
    }
}
enum MyValue: Codable {
    case string(String)
    case innerItem(Int)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(Int.self) {
            self = .innerItem(x)
            return
        }
        throw DecodingError.typeMismatch(MyValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .innerItem(let x):
            try container.encode(x)
        }
    }
}

struct InnerItem: Codable {
    let type, id, name: String
    
    enum CodingKeys: String, CodingKey {
        case type = "__type"
        case id = "Id"
        case name = "Name"
    }
}

struct StoreStruct: Codable {
    let id: Int?
    let name, email: String?
    let profileImage: String?
    let mobile, status, type, cityID: String?
    let lat, lng, address, description: String?
    let fullDescription, openingTime, closingTime, categoryID: String?
    let facebook, twitter, instagram, deliveryCost: String?
    let jobID: String?
    let close, statusNow: String?
    let category: CategoryStruct?
    let city: CityStruct?
    let jobs: CityStruct?
    let subCategories: [CategoryStruct]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case profileImage = "profile_image"
        case mobile, status, type
        case cityID = "city_id"
        case lat, lng, address, description
        case fullDescription = "full_description"
        case openingTime = "opening_time"
        case closingTime = "closing_time"
        case categoryID = "category_id"
        case facebook, twitter, instagram
        case deliveryCost = "delivery_cost"
        case jobID = "job_id"
        case close
        case statusNow = "status_now"
        case  category, city, jobs
        case subCategories = "sub_categories"
    }

//    let id: Int?
//    let name, email: String?
//    let profileImage: String?
//    let status: String?
//    let mobile , type, country: String?
//    let lat, lng, description, fullDescription: String?
//    let openingTime, closingTime, categoryID, address: String?
//    let facebook, twitter, instagram: String?
//    let category: CategoryStruct?
//    let notification: [Notification]?
//    let city : CityStruct?
//    enum CodingKeys: String, CodingKey {
//        case id, name, email
//        case profileImage = "profile_image"
//        case mobile, status, type, country, lat, lng, description
//        case fullDescription = "full_description"
//        case openingTime = "opening_time"
//        case closingTime = "closing_time"
//        case categoryID = "category_id"
//        case address, facebook, twitter
//        case instagram = "Instagram"
//        case notification, category,city
//    }
}
struct Notification: Codable {
    let id: Int?
    let userID: String?
    let companyID: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case companyID = "company_id"
        case message
    }
}
//struct Category: Codable {
//    let id: Int?
//    let logo: String?
//    let isParent: String?
//    let subCategory: [CategoryStruct]?
//    let title: String?
//    
//    enum CodingKeys: String, CodingKey {
//        case id, logo
//        case isParent = "is_parent"
//        case subCategory = "sub_category"
//        case title
//    }
//}



struct ProductsObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let ProductStruct: [ProductStruct]?
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case ProductStruct = "items"
    }
}
struct ProductObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let ProductStruct: ProductStruct?
    enum CodingKeys: String, CodingKey {
        case status, code,message
        case ProductStruct = "items"
    }
}
struct ProductStruct: Codable {
    let id: Int?
    let userID, categoryID, price, priceAfterOffer: String?
    let availability, status, delete: String?
    let image: String?
    let isFavourite: Int?
    let inCart,views: Int?
    let shareLink: String?

    let name, description: String?
    let favourite: String?
    let category: CategoryStruct?
    let attatchments: [Attatchment]?
    let store: Store?
    let translations: [TranslationStruct]?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case views
        case inCart = "in_cart"
        case shareLink = "share_link"

        case categoryID = "category_id"
        case price
        case priceAfterOffer = "price_after_offer"
        case availability,translations, status, delete, image
        case isFavourite = "is_favourite"
        case name, description, favourite, category, attatchments, store
    }
}
struct TranslationStruct: Codable {
    let id: Int?
    let productID, locale, name, description: String?
    let createdAt, updatedAt: String?
    let deletedAt: String?
        enum CodingKeys: String, CodingKey {
        case id
        case productID = "product_id"
        case locale, name, description
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
    }
}

struct Attatchment: Codable {
    let id: Int?
    let productID: String?
    let image: String?
    let createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case productID = "product_id"
        case image
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
struct Store: Codable {
    let id: Int?
    let name, email, profileImage, mobile: String?
    let status, type, cityID, lat: String?
    let lng, address, description: String?
    let fullDescription: String?
    let openingTime, closingTime, categoryID: String?
    let facebook, twitter, instagram: String?
    let deliveryCost, jobID: String?
    let close: String?
    let category: CategoryStruct?
    let city: CityStruct?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case profileImage = "profile_image"
        case mobile, status, type
        case cityID = "city_id"
        case lat, lng, address, description
        case fullDescription = "full_description"
        case openingTime = "opening_time"
        case closingTime = "closing_time"
        case categoryID = "category_id"
        case facebook, twitter
        case instagram = "Instagram"
        case deliveryCost = "delivery_cost"
        case jobID = "job_id"
        case close, category, city
    }
}
struct CartObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: cartItems?
}

struct cartItems: Codable {
    let totalPrice: Double?
    let costDelivery: String?
    let cart: [Cart]?
    
    enum CodingKeys: String, CodingKey {
        case totalPrice = "total_price"
        case costDelivery = "cost_Delivery"
        case cart
    }
}

struct Cart: Codable {
    let id: Int?
    let userID, productID, quantity, createdAt: String?
    let storeName: String?
    let deliveryCost: Double?
    let product: ProductStruct?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case productID = "product_id"
        case quantity
        case createdAt = "created_at"
        case storeName = "store_name"
        case deliveryCost = "delivery_cost"
        case product
    }
}

struct customerOrderObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemCustomerOrder]?
}

struct ItemCustomerOrder: Codable {
    let id: Int?
    let userID, storeID, deliveryCompanyID: String?
    let date, deliveryDate, totalPrice: String?
    let deliveryAddress, deliveryMethod, paymentMethod, status: String?
    let referenceID, createdAt, changeStatus, storeName: String?
    let customerName: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case storeID = "store_id"
        case deliveryCompanyID = "delivery_company_id"
        case date
        case deliveryDate = "delivery_date"
        case totalPrice = "total_price"
        case deliveryAddress = "delivery_address"
        case deliveryMethod = "delivery_method"
        case paymentMethod = "payment_method"
        case status
        case referenceID = "reference_id"
        case createdAt = "created_at"
        case changeStatus = "change_Status"
        case storeName = "store_name"
        case customerName = "customer_name"
    }
}
struct sliderObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [sliderItem]?
}

struct sliderItem: Codable {
    let id: Int?
    let type, status: String?
    let image: String?
    let position, order: String?
    let link: String?
    let title: String?
}
struct OrderObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: OrderItems?
}
struct CompanyItem: Codable {
    let id: Int?
    let mobile, email: String?
    let logo: String?
    let status, url, updatedAt, createdAt: String?
    let deletedAt: String?
    let name, address: String?
    let translations: [Translation]?
    
    enum CodingKeys: String, CodingKey {
        case id, mobile, email, logo, status, url
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case deletedAt = "deleted_at"
        case name, address, translations
    }
}

struct OrderItems: Codable {
    let id: Int?
    let userID, storeID, deliveryCompanyID: String?
    let date, deliveryDate, totalPrice: String?
    let deliveryAddress, deliveryMethod, paymentMethod, status: String?
    let referenceID, createdAt, changeStatus, storeName: String?
    let customerName: String?
    let products: [ProductElement]?
    let company: CompanyItem?
    let deliveryCost: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case storeID = "store_id"
        case deliveryCompanyID = "delivery_company_id"
        case date
        case deliveryCost = "delivery_cost"

        case deliveryDate = "delivery_date"
        case totalPrice = "total_price"
        case deliveryAddress = "delivery_address"
        case deliveryMethod = "delivery_method"
        case paymentMethod = "payment_method"
        case status
        case referenceID = "reference_id"
        case createdAt = "created_at"
        case changeStatus = "change_Status"
        case storeName = "store_name"
        case customerName = "customer_name"
        case products
        case company

    }
}

struct ProductElement: Codable {
    let id: Int?
    let orderID, productID, quantity: String?
    let product: ProductProduct?
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderID = "order_id"
        case productID = "product_id"
        case quantity, product
    }
}

struct ProductProduct: Codable {
    let id: Int?
    let userID, categoryID, price, priceAfterOffer: String?
    let availability, status, delete: String?
    let image: String?
    let isFavourite, views, inCart: Int?
    let name, description: String?
    let favourite: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case categoryID = "category_id"
        case price
        case priceAfterOffer = "price_after_offer"
        case availability, status, delete, image
        case isFavourite = "is_favourite"
        case views
        case inCart = "in_cart"
        case name, description, favourite
    }
}



struct ServiceProviderObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemServiceProvider]?
}

struct ItemServiceProvider: Codable {
    let id: Int?
    let name, email: String?
    let profileImage: String?
    let mobile: String?
    let status: String?
    let type, cityID, lat, lng: String?
    let address: String?
    let description, fullDescription, openingTime, closingTime: String?
    let categoryID, facebook, twitter, instagram: String?
    let deliveryCost: String?
    let jobID, close, statusNow: String?
    let category: String?
    let city: CityStruct?
    let jobs: CityStruct?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case profileImage = "profile_image"
        case mobile, status, type
        case cityID = "city_id"
        case lat, lng, address, description
        case fullDescription = "full_description"
        case openingTime = "opening_time"
        case closingTime = "closing_time"
        case categoryID = "category_id"
        case facebook, twitter, instagram
        case deliveryCost = "delivery_cost"
        case jobID = "job_id"
        case close
        case statusNow = "status_now"
        case category, city, jobs
    }
}

struct SliderAdsObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [SliderAdsItem]?
}

struct SliderAdsItem: Codable {
    let id: Int?
    let type, status: String?
    let image: String?
    let position, order: String?
    let link: String?
    let title: String?
}
struct OfferObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemOffer]?
}

struct ItemOffer: Codable {
    let id: Int?
    let userID, categoryID, price, priceAfterOffer: String?
    let availability, status, delete: String?
    let image: String?
    let isFavourite, views, inCart: Int?
    let shareLink: String?
    let name, description: String?
    let favourite: String?
    let translations: [Translation]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case categoryID = "category_id"
        case price
        case priceAfterOffer = "price_after_offer"
        case availability, status, delete, image
        case isFavourite = "is_favourite"
        case views
        case inCart = "in_cart"
        case shareLink = "share_link"
        case name, description, favourite, translations
    }
}
struct SearchObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemSearch]?
}

struct ItemSearch: Codable {
    let id: Int?
    let name: String?
    let type: Int?
    let image: String?
}
struct SettingObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: ItemsSetting?
}

struct ItemsSetting: Codable {
    let id: Int?
    let url: String?
    let logo: String?
    let adminEmail: String?
    let appStoreURL, playStoreURL: String?
    let infoEmail, mobile, phone: String?
    let facebook, twitter, linkedIn, instagram: String?
    let googlePlus: String?
    let paginate, latitude, longitude: String?
    let image: String?
    let vedio, joinUs: String?
    let note: String?
    let coloredPrice, blackWhitePrice: String?
    let createdAt: String?
    let updatedAt: String?
    let aboutUs, privacy, terms: AboutUs?
    let title, joinDescription, description, address: String?
    let keyWords: String?
    
    enum CodingKeys: String, CodingKey {
        case id, url, logo
        case adminEmail = "admin_email"
        case appStoreURL = "app_store_url"
        case playStoreURL = "play_store_url"
        case infoEmail = "info_email"
        case mobile, phone, facebook, twitter
        case linkedIn = "linked_in"
        case instagram
        case googlePlus = "google_plus"
        case paginate, latitude, longitude, image, vedio
        case joinUs = "join_us"
        case note
        case coloredPrice = "colored_price"
        case blackWhitePrice = "blackWhite_price"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case aboutUs, privacy, terms, title
        case joinDescription = "join_description"
        case description, address
        case keyWords = "key_words"
    }
}

struct AboutUs: Codable {
    let id: Int?
    let image: String?
    let views, createdAt, updatedAt: String?
    let deletedAt: String?
    let title, slug, description, keyWords: String?
    let translations: [Translation]?
    
    enum CodingKeys: String, CodingKey {
        case id, image, views
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case title, slug, description
        case keyWords = "key_words"
        case translations
    }
}
