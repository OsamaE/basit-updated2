//
//  TAConstant.swift
//  
//
//  Created by Tareq Safia on 11/4/17.
//  Copyright © 2017 Tareq Safia. All rights reserved.
//
import Foundation
import UIKit
struct TAConstant
{
    
    static let kAPIBaseURL                                 = "https://basit.app//api/"
    static let coffee_shops                                = kAPIBaseURL +  "coffee_shops"
    static let ProductsUrl                                 = kAPIBaseURL + "products"
    static let ClearUrl                                 = kAPIBaseURL + "empty"

    static let add_cartURL                                 = kAPIBaseURL + "add_cart"
    static let buy_voucher                                 = kAPIBaseURL + "buy_voucher"
    static let vouchers                                    = kAPIBaseURL +  "vouchers"
    static let rating                                      = kAPIBaseURL + "rating"
    static let cities                                      = kAPIBaseURL + "cities"
    static let contact                                     = kAPIBaseURL + "contact"
    static let promotions                                  = kAPIBaseURL + "promotions"
    static let cart                                        = kAPIBaseURL + "cart"
    static let update_cart                                 = kAPIBaseURL + "update_cart"
    static let notification                                = kAPIBaseURL + "notification"
    static let notifications                               = kAPIBaseURL + "notifications"
    static let wallet                                      = kAPIBaseURL + "wallet"
    static let transfer                                    = kAPIBaseURL + "transfer"
    static let checkout                                    = kAPIBaseURL + "checkout"
    static let orders                                      = kAPIBaseURL + "orders"
    static let update_profile                              = kAPIBaseURL + "update_profile"
    static let app_details                                 = kAPIBaseURL + "app_details"
    static let login                                       = kAPIBaseURL + "login"
    static let delete_cart                                 = kAPIBaseURL + "delete_cart"
    static let change_password                             = kAPIBaseURL + "change_password"
    static let refreshSideMenu = NSNotification.Name(rawValue: "refreshSideMenu")
   
    
}

