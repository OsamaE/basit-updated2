//
//  PleacHolder.swift
//  new
//
//  Created by musbah on 7/25/18.
//  Copyright © 2018 fit. All rights reserved.
//

import UIKit

@IBDesignable
    
    extension UITextField{
        
        @IBInspectable
        var placeHolderColor: UIColor? {
            get {
                return self.placeHolderColor
            }
            set {
                self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedString.Key: newValue!])
            }
        }
    }



