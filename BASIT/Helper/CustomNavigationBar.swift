//
//  CustomNavigationBar.swift
//  dabberly
//
//  Created by ahmed on 6/7/18.
//  Copyright © 2018 ahmed. All rights reserved.
//

import UIKit

class CustomNavigationBar: UINavigationController {
    
    var FilterBtn: UIBarButtonItem?
    var EditBtn: UIBarButtonItem?
    var SearchBtn: UIBarButtonItem?
    var CartBtn: UIBarButtonItem?
    var DeletBtn: UIBarButtonItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationBar.barTintColor = self.UIColorFromRGB(rgbValue: 0x463F5F) deletic
        self.navigationBar.setBackgroundImage(UIImage(named:"appbarbg"),for: .default)
        
        let ic_filter: UIImage? = UIImage(named:"ic_filter")?.withRenderingMode(.alwaysOriginal)
        
        FilterBtn = UIBarButtonItem(image: ic_filter, style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        EditBtn = UIBarButtonItem(image: UIImage(named:"ic_mode_edit")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        
        DeletBtn = UIBarButtonItem(image: UIImage(named:"deletic")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))

        CartBtn = UIBarButtonItem(image: UIImage(named:"ic_cart")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))

        SearchBtn = UIBarButtonItem(image: UIImage(named:"ic_search")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(didClickRightButton))
        SearchBtn?.tag = 33
        
        FilterBtn?.tag = 44
        
        /*
         When you are refering controller level navigation bar below method will work
         */
        //        setNavigationBar(withType: .withCustomBackButton)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    func setBtnTitle(title :String) -> UIBarButtonItem{
        let BarButton: UIBarButtonItem = UIBarButtonItem.init(title: title, style: .plain, target: self, action: #selector(didClickRightButton))
        BarButton.tintColor = "649AB8".color
        return BarButton
    }
    
    func setLogotitle(sender :UIViewController){
        let logo = UIImage(named: "logoHeader.png")
        let imageView = UIImageView(image:logo)
        sender.navigationItem.titleView = imageView
    }
    
    func setCustomBackButtonWithdismiss(sender :UIViewController){
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"right_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonActionWithdismiss))
            
        } else{
            let back: UIImage? = UIImage(named:"left_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonActionWithdismiss))
            
        }
    }
    func setCustomBackButtonForViewController(sender :UIViewController){
        if MOLHLanguage.isRTLLanguage() {
            let back: UIImage? = UIImage(named:"right_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
            
        } else{
            let back: UIImage? = UIImage(named:"left_back")?.withRenderingMode(.alwaysOriginal)
            
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        }
        
        
    }
    func setRightButtons (_ buttons: NSArray,sender : UIViewController){
        sender.navigationItem.rightBarButtonItems = buttons as? [UIBarButtonItem]
        
    }
    @objc func didClickRightButton(_sender: UIBarButtonItem) {
        let ViewController = self.viewControllers.last as! SuperViewController
        ViewController.didClickRightButton(_sender: _sender)
        
    }
    @objc func backButtonAction(_sender: UIBarButtonItem) {
        let ViewController = self.viewControllers.last as! SuperViewController
        ViewController.backButtonAction(_sender: _sender)
        
    }
    @objc func backButtonActionWithdismiss(_sender: UIBarButtonItem) {
        let ViewController = self.viewControllers.last as! SuperViewController
        ViewController.backButtonActionWithdismiss(_sender: _sender)
        
    }
    
    func setTitle (_ title: NSString,sender : UIViewController){
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "Arial", size: 20)!
        ]
        sender.navigationController?.navigationBar.titleTextAttributes = attrs
        //        sender.navigationController?.navigationBar.topItem?.title = title as String
        sender.navigationItem.title = title as String
        
        sender.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : "649AB8".color]
        
////        let items = self.tabBarController?.tabBar.items
////        let tabItem = items![1]
//        tabItem.title = ""
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}
