//
//  TypeUserVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class TypeUserVC: SuperViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonWithdismiss(sender: self)
        navigation.setTitle("Sign Up".localized as NSString, sender: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didSignUpfirst(_ sender: UIButton) {
        let vc:SignupVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didSignUpSecond(_ sender: Any) {
        let vc:SignupSecondVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didSignUpthird(_ sender: UIButton) {
        let vc:SignupThirdVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }

    @objc func addTapped() {
    }    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
