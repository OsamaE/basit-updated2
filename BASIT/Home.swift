//
//  Home.swift
//  BASIT
//
//  Created by ahmed on 1/17/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class Home: SuperViewController,UICollectionViewDelegate,HeaderHomeDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var heightHeader: CGFloat? = 300
    
    var CategoryArray = [CategoryStruct]()
    var sliderArray = [sliderItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        Localizer.DoTheExchange()
        if MOLHLanguage.isRTLLanguage() {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft

        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight

        }
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Home".localized as NSString, sender: self)
        navigation.setRightButtons([navigation.SearchBtn as Any], sender: self)

        self.collectionView.registerCell(id: "CategoriesCell")

        _ = WebRequests.setup(controller: self).prepare(query: "getCategories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                
                
                let object =  try JSONDecoder().decode(CategoriesObject.self, from: response.data!)
                self.CategoryArray = object.Categories!
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "slider", method: HTTPMethod.get).start(){ (response, error) in
            do {
                
                
                let object =  try JSONDecoder().decode(sliderObject.self, from: response.data!)
                self.sliderArray = object.items!
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let vc:SearchVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func swiftyHeaderHomeDidTap(_ sender: Int) {
        if sliderArray[sender].type == "0"{ //store
            let store = StoreStruct.init(id: Int(sliderArray[sender].link!), name: nil, email: nil, profileImage: nil, mobile: nil, status: nil, type: nil, cityID: nil, lat: nil, lng: nil, address: nil, description: nil, fullDescription: nil, openingTime: nil, closingTime: nil, categoryID: nil, facebook: nil, twitter: nil, instagram: nil, deliveryCost: nil, jobID: nil, close: nil, statusNow: nil, category: nil, city: nil, jobs: nil, subCategories: nil)
            let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
            vc.StoreInfo = store
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if sliderArray[sender].type == "1"{ // product
            let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
            let prod = ProductStruct.init(id: Int(sliderArray[sender].link!), userID: "", categoryID: "", price: "", priceAfterOffer: "", availability: "", status: "", delete: "", image: "", isFavourite: nil,inCart:nil,views:nil,shareLink:nil, name: "", description: "", favourite: "", category: nil, attatchments: nil, store: nil, translations: nil)
            vc.ProductInfo = prod
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        guard let url = URL(string: sliderArray[sender].link!) else { return }
        UIApplication.shared.open(url)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if CategoryArray.count == 0 {
            self.collectionView.setEmptyMessage("No Data".localized)
        } else {
            self.collectionView.restore()
        }
        return self.CategoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let catogery = self.CategoryArray[indexPath.row]
        let cell :CategoriesCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.lb_title.text = catogery.title
        cell.img_thumb.sd_custom(url: catogery.logo!)

        return cell
        
//        return UICollectionViewCell()
    }
    
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let size = CGSize(width: screenWidth/3.1, height: screenWidth/2.5)
        return size
        
        //return CGSize()
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: heightHeader!)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderHome", for: indexPath as IndexPath) as! HeaderHome
            headerView.delegate = self
            headerView.InitCarousel(sendArray: self.sliderArray );
            
//            if Int(heightHeader!) < 400{
//                headerView.optionsView.isHidden = true
//            }else{
//                headerView.optionsView.isHidden = false
//                
//            }
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            
            assert(false, "Unexpected element kind")
            
            
        default:
            
            fatalError("Unexpected element kind")
        }
        fatalError("Unexpected element kind")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc:SubcategoryVC = AppDelegate.sb_main.instanceVC()
        vc.CategoryArray = self.CategoryArray[indexPath.row].subCategory!
        vc.Title_cat = self.CategoryArray[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let navigation = self.navigationController as! CustomNavigationBar
//        navigation.setRightButtons([navigation.SearchBtn as Any], sender: self)
//        navigation.setLogotitle(sender: self)
//        let items = self.tabBarController?.tabBar.items
//        let tabItem = items![0]
//        tabItem.title = ""
        let headerNib = UINib.init(nibName: "HeaderHome", bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderHome")
//
//        loadDate()
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
