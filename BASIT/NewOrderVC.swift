//
//  NewOrderVC.swift
//  BASIT
//
//  Created by ahmed on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Gallery

class NewOrderVC: SuperViewController,GalleryControllerDelegate
{
   
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tf_nameEn: UITextField!
    @IBOutlet weak var tf_nameAr: UITextField!
    @IBOutlet weak var tf_desEn: UITextField!
    @IBOutlet weak var tf_desAr: UITextField!
    @IBOutlet weak var tf_curPrice: UITextField!
    @IBOutlet weak var tf_oldPrice: UITextField!
    @IBOutlet weak var tf_subCat: UITextField!
    
    @IBOutlet weak var tf_qun: UITextField!

    var SubCategorArray = [CategoryStruct]()
    var ImageList = [UIImage]()
    var ProductInfo: ProductStruct?

    var categoryIndex:Int?
    var IsEdit:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Product".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        if IsEdit {
            
            tf_nameEn.text = self.ProductInfo?.translations![0].name
            tf_nameAr.text = self.ProductInfo?.translations![1].name
            tf_desEn.text = self.ProductInfo?.translations![0].description
            tf_desAr.text = self.ProductInfo?.translations![1].description
            tf_oldPrice.text = self.ProductInfo?.priceAfterOffer
            tf_curPrice.text = self.ProductInfo?.price
            self.categoryIndex =  Int((self.ProductInfo?.categoryID!)!)
        }
        _ = WebRequests.setup(controller: self).prepare(query: "getSubCategories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CategoriesObject.self, from: response.data!)
                self.SubCategorArray = Status.Categories!
                if self.IsEdit {
                    let catID = Int((self.ProductInfo?.categoryID)!)
                    let indexid = self.SubCategorArray.index(where: { $0.id == catID})

                    self.tf_subCat.text = self.SubCategorArray[indexid!].title
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func selectImage(_ sender: Any) {
        let gallery = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        gallery.delegate = self
        present(gallery, animated: true, completion: nil)

    }
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self!.ImageList = resolvedImages.compactMap({ $0 })
            self!.collectionView.reloadData()

            })
        controller.dismiss(animated: true, completion: nil)

    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func selectCategory(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Category".localized, rows: self.SubCategorArray.map { $0.title as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tf_subCat.text = Value as? String
            }
            self.categoryIndex = self.SubCategorArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func didSaveButtonPressed(_ sender: UIButton) {
        guard Helper.isConnectedToNetwork() else {
            
            //self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let nameEn = self.tf_nameEn.text, !nameEn.isEmpty else{
            self.showAlert(title: "Error".localized, message: "English Name require".localized)
            return
        }
        guard let nameAr = self.tf_nameAr.text, !nameAr.isEmpty else{
            self.showAlert(title: "Error".localized, message: "Arabic Name require".localized)
            return
        }
        guard let qun = self.tf_qun.text, !qun.isEmpty else{
            self.showAlert(title: "Error".localized, message: "Quantity require".localized)
            return
        }
        
        
        guard let desEn = self.tf_desEn.text, !desEn.isEmpty else{
            self.showAlert(title: "Error".localized, message: "English Description required".localized)
            return
        }
        guard let desAr = self.tf_desAr.text, !desAr.isEmpty else{
            self.showAlert(title: "Error".localized, message: "Arabic Description required".localized)
            return
        }
//        guard let Curr_Price = self.tf_curPrice.text, !Curr_Price.isEmpty else{
//            self.showAlert(title: "Error".localized, message: "Current Price required".localized)
//            return
//        }

        guard let Old_Price = self.tf_oldPrice.text, !Old_Price.isEmpty else{
            self.showAlert(title: "Error".localized, message: "Price required".localized)
            return
        }
        if self.categoryIndex == nil{
            self.showAlert(title: "Error".localized, message: "Sub Category required".localized)
            return
        }
        if ImageList.count == 0 && IsEdit == false {
            self.showAlert(title: "Error".localized, message: "Image required".localized)
            return

        }
        var parameters: [String: Any] = [:]
        
        parameters["name_ar"] = nameAr
        parameters["name_en"] = nameEn
        parameters["description_ar"] = desAr
        parameters["description_en"] = desEn
        parameters["quantity"] = qun
        parameters["price_after_offer"] = self.tf_curPrice.text!
        parameters["price"] = self.tf_oldPrice.text!
        parameters["categories_id"] = self.categoryIndex?.description
  
        parameters["availability"] = "1"
        parameters["status"] = "active"
        self.showIndicator()
        var link = "addProduct"
        if IsEdit {
            link = "editProduct/\(self.ProductInfo?.id! ?? 0)"
        }
        WebRequests.sendPostMultipartRequestWithMultiImgParam(url: TAConstant.kAPIBaseURL + link, parameters: parameters as! [String : String], imges:ImageList ,withName:"images") {(response, error) in
            self.hideIndicator()
            

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status! {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.showAlert(title: "Error".localized, message:Status.message!)

                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewOrderVC : UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return    ImageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyImageCell", for: indexPath) as! MyImageCell
        cell.myImage.image = ImageList[indexPath.row]

        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 80 , height: 80)
    }
    
    
}
class MyImageCell: UICollectionViewCell {
    @IBOutlet weak var myImage: UIImageView!
}
