//
//  SignupVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps
import GooglePlaces
import GooglePlacePicker


class SignupThirdVC: SuperViewController ,SubCategoryDelgate,GMSPlacePickerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func SelectedDone(_ sender: [CategoryStruct]) {
        
    }
    
    
    
    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var tf_jobTitle: UITextField!
    
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_Loaction: UITextField!
    @IBOutlet weak var tf_Name: UITextField!
    @IBOutlet weak var tf_Mobile: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var tf_face: UITextField!
    @IBOutlet weak var tf_twitter: UITextField!
    @IBOutlet weak var insta: UITextField!

    @IBOutlet weak var tf_Password: UITextField!
    
    
    var SubCategorArray = [CategoryStruct]()
    var CategorArray = [CategoryStruct]()
    var JobArray = [ItemJob]()
    var CityArray = [CityStruct]()
    var job_id :Int!
    var city_id :Int!
    var lat:Float!
    var lng:Float!
    var categoryIndex :Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Join Us".localized as NSString, sender: self)
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "getJobs", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(JobsObject.self, from: response.data!)
                self.JobArray = Status.items!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func editImageProfile(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        self.present(vc, animated: true)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.img_Profile.image = image
        let imgData = (image.jpegData(compressionQuality: 0.1))!
        UserDefaults.standard.set(imgData, forKey: "imgData")
    }
    
    @IBAction func SelectjobTitle(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "getJobs".localized, rows: self.JobArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.tf_jobTitle.text = Value as? String
            }
            self.job_id = self.JobArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.tf_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    
    @IBAction func selectLocation(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        self.tf_Loaction.text = place.formattedAddress ?? "\(place.coordinate.latitude);\(place.coordinate.longitude)"
        self.lat = Float(place.coordinate.latitude)
        self.lng = Float(place.coordinate.longitude)
        //        print("Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    
    
    
    
    @IBAction func didSignUpButtonPressed(_ sender: UIButton) {
        guard Helper.isConnectedToNetwork() else {
            
            //self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let name = self.tf_Name.text, !name.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Name required".localized)
            return
        }
        guard let email = self.tf_Email.text, !email.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Email required".localized)
            return
        }
        guard let mobile = self.tf_Mobile.text, !mobile.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Mobile required".localized)
            return
        }
        guard let password = self.tf_Password.text, !password.isEmpty else{
            self.showAlert(title:  "Erorr".localized, message: "Password required".localized)
            return
        }
        guard let cityID = self.city_id else{
            self.showAlert(title:  "Erorr".localized, message: "City required".localized)
            return
        }

        guard let jobID = self.job_id else{
            self.showAlert(title:  "Erorr".localized, message: "Jop required".localized)
            return
        }
        
        guard let lng = self.lng else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        guard let lat = self.lat else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        guard let address = self.tf_Loaction.text else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        
        guard let imgProfile = self.img_Profile.image else{
            self.showAlert(title: "Erorr".localized, message: "image Profile required".localized)
            return
        }
        
        
        
        var parameters: [String: Any] = [:]
        
        parameters["name"] = name
        parameters["email"] = email
        parameters["mobile"] = mobile
        parameters["password"] = password
        parameters["confirm_password"] = password
        parameters["city_id"] = cityID.description
        parameters["job_id"] = jobID.description
        parameters["lng"] = lng.description
        parameters["lat"] = lat.description
        parameters["address"] = address
        parameters["type"] = "2"
        parameters["device_type"] = "ios"
        parameters["fcm_token"] = Helper.device_token
        parameters["instagram"] = self.insta.text!
        parameters["twitter"] = self.tf_twitter.text!
        parameters["facebook"] = self.tf_face.text!
        self.showIndicator()

        WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.kAPIBaseURL + "signUp", parameters: parameters as! [String : String], img:imgProfile ,withName:"profile_image") {(response, error) in
            self.hideIndicator()

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if !Status.status! {
                
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                CurrentUser.userInfo = Status.items
                print("SessionManager.shared.session", CurrentUser.userInfo as Any)
                if CurrentUser.userInfo != nil{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
                    self.present(vc, animated: true, completion: nil)
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        //        _ = WebRequests.setup(controller: self).prepare(query: "signUp", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
        //        }
        
    }
    
    
}
