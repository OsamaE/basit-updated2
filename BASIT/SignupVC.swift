//
//  SignupVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class SignupVC: SuperViewController ,GMSPlacePickerViewControllerDelegate{
    
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_Loaction: UITextField!
    @IBOutlet weak var tf_Password: UITextField!
    @IBOutlet weak var tf_ConPassword: UITextField!
    @IBOutlet weak var tf_Name: UITextField!
    @IBOutlet weak var tf_Mobile: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    

    
    var CityArray = [CityStruct]()
    var city_id :Int!
    var lat:Float!
    var lng:Float!
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Sign Up".localized as NSString, sender: self)
        
        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        self.tf_Loaction.text = place.formattedAddress ?? "\(place.coordinate.latitude);\(place.coordinate.longitude)"
        self.lat = Float(place.coordinate.latitude)
        self.lng = Float(place.coordinate.longitude)
        //        print(";Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    @IBAction func selectCity(_ sender: UIButton) {

        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {

            self.tf_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didSignUpButtonPressed(_ sender: UIButton) {
        guard Helper.isConnectedToNetwork() else {
            
            //self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let name = self.tf_Name.text, !name.isEmpty else{
            self.showAlert(title: "erorr", message: "Name required".localized)
            return
        }
        guard let email = self.tf_Email.text, !email.isEmpty else{
            self.showAlert(title: "erorr", message: "Email required".localized)
            return
        }
        guard let mobile = self.tf_Mobile.text, !mobile.isEmpty else{
            self.showAlert(title: "erorr", message: "Moblie required".localized)
            return
        }
        guard let password = self.tf_Password.text, !password.isEmpty else{
            self.showAlert(title: "erorr", message: "Password required".localized)
            return
        }
        guard self.city_id != nil else{
            self.showAlert(title: "erorr", message: "City required".localized)
            return
        }
  
    
        
        guard self.lng != nil else{
            self.showAlert(title: "erorr", message: "Address required".localized)
            return
        }
        guard self.lat != nil else{
            self.showAlert(title: "erorr", message: "Address required".localized)
            return
        }
        guard self.tf_Loaction.text != nil else{
            self.showAlert(title: "erorr", message: "Address required".localized)
            return
        }
      
        let parameters: [String: Any] = ["name": tf_Name.text!,"email": tf_Email.text!,"mobile": tf_Mobile.text!,"password": tf_Password.text!,"confirm_password":tf_Password.text!,"city_id": self.city_id!,"lng" : self.lng!,"lat" : self.lat!,"address":self.tf_Loaction.text!, "type" :"0","device_type" : "ios","fcm_token" : Helper.device_token]
        
        _ = WebRequests.setup(controller: self).prepare(query: "signUp", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
           
            
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if !Status.status! {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                CurrentUser.userInfo = Status.items
                print("SessionManager.shared.session", CurrentUser.userInfo as Any)
                if CurrentUser.userInfo != nil{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
                    self.present(vc, animated: true, completion: nil)
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        
        
        
        

        }

    }


}
