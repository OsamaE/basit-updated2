//
//  SignupVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps
import GooglePlaces
import GooglePlacePicker


class EditUserVC: SuperViewController,GMSPlacePickerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var tf_Name: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var tf_Mobile: UITextField!
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_Loaction: UITextField!
    
    var userProfile : UserStruct?
    
    
    var CityArray = [CityStruct]()
    var city_id :Int!
    var lat:Float!
    var lng:Float!
    var categoryIndex :Int!
    var Indexcategory :Int!
    
    var catogerylist = [CategoryStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Edit Profile".localized as NSString, sender: self)
        
        lat = Float(userProfile!.lat!)
        lng = Float(userProfile!.lng!)
        city_id = Int(userProfile!.cityID!)
//        categoryIndex = Int(userProfile!.categoryID!)
        tf_Name.text = userProfile?.name
        tf_Email.text = userProfile?.email
        tf_Mobile.text = userProfile?.mobile
        tf_city.text = userProfile?.city?.name
        tf_Loaction.text = userProfile?.address
        img_Profile.sd_custom(url: (userProfile?.profileImage)!)
//        catogerylist = (userProfile?.category?.subCategory)!
        
        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func editImageProfile(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        self.present(vc, animated: true)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.img_Profile.image = image
        let imgData = (image.jpegData(compressionQuality: 0.1))!
        UserDefaults.standard.set(imgData, forKey: "imgData")
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
    }
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        self.tf_Loaction.text = place.formattedAddress ?? "\(place.coordinate.latitude);\(place.coordinate.longitude)"
        self.lat = Float(place.coordinate.latitude)
        self.lng = Float(place.coordinate.longitude)
        //        print("Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.tf_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    
    @IBAction func didSignUpButtonPressed(_ sender: UIButton) {
        guard Helper.isConnectedToNetwork() else {
            
            //self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let name = self.tf_Name.text, !name.isEmpty else{
            self.showAlert(title: "erorr", message: "Name required")
            return
        }
        guard let email = self.tf_Email.text, !email.isEmpty else{
            self.showAlert(title: "erorr", message: "Email required")
            return
        }
        guard let mobile = self.tf_Mobile.text, !mobile.isEmpty else{
            self.showAlert(title: "erorr", message: "moblie required")
            return
        }
      
        
        guard let cityID = self.city_id else{
            self.showAlert(title: "erorr", message: "address required")
            return
        }
        
        guard let lng = self.lng else{
            self.showAlert(title: "erorr", message: "address required")
            return
        }
        guard let lat = self.lat else{
            self.showAlert(title: "erorr", message: "address required")
            return
        }
        guard let address = self.tf_Loaction.text else{
            self.showAlert(title: "erorr", message: "address required")
            return
        }
        
        guard let imgProfile = self.img_Profile.image else{
            self.showAlert(title: "erorr", message: "img Profile Time required")
            return
        }
        
        
        
        var parameters: [String: Any] = [:]
        
        parameters["name"] = name
        parameters["email"] = email
        parameters["mobile"] = mobile
        parameters["city_id"] = cityID.description
        parameters["lng"] = lng.description
        parameters["lat"] = lat.description
        parameters["address"] = address
        
        self.showIndicator()
        WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.kAPIBaseURL + "editUser", parameters: parameters as! [String : String], img:imgProfile ,withName:"profile_image") {(response, error) in
            self.hideIndicator()

            do {
                
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status!  {
                    
                    
                    do {
                        let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                        CurrentUser.userInfo = Status.items
                        self.navigationController?.popViewController(animated: true)
                        
                    } catch let jsonErr {
                        print("Error serializing  respone json", jsonErr)
                    }
                    
                }else{
                    self.showAlert(title: "Done".localized, message: Status.message!)
                    
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            //            if status.status{
            //
            //            }
        }
        
        
    }
    
    
}
