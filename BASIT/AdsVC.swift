//
//  SRAddsVC.swift
//  SeasonSport
//
//  Created by sos on 9/19/18.
//  Copyright © 2018 Somia Radi. All rights reserved.
//
struct AdsObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [ItemAds]?
}

struct ItemAds: Codable {
    let id: Int?
    let image: String?
    let link: String?
    let type: String?
    let position, orderBy: String?
    let typeFor, descriptions: String?
    
    enum CodingKeys: String, CodingKey {
        case id, image, link, type, position
        case orderBy = "order_by"
        case typeFor = "type_for"
        case descriptions
    }
}
import UIKit

class SRAddsVC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var collectionview: UICollectionView!
//    var element : [[String:Any]] = []
    var timer:Timer? = nil
//    var datasource: [UIImage]?
    var sliderArray = [ItemAds]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        collectionview.isPagingEnabled = true
        
        _ = WebRequests.setup(controller: self).prepare(query: "settings", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(SettingObject.self, from: response.data!)
                CurrentUser.Setting = Status.items!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        // autoScrollImageSlider()
        // Do any additional setup after loading the view.
    }
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = collectionview {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < sliderArray.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
        
    }
    @IBAction func btn_skip(_ sender: UIButton) {
        if CurrentUser.userInfo != nil{
            Go_tomain()

        }else {
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            self.present(vc , animated: true)
        }
//
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionview {
            let pageWidth = scrollView.bounds.width
            let pageFraction = scrollView.contentOffset.x / pageWidth
            let current = Int(round(pageFraction))
            self.pager.currentPage   = current
        }
    }
    func Go_tomain()  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
        self.present(vc, animated: true, completion: nil)

    }
    func getData()  {
        
        _ = WebRequests.setup(controller: self).prepare(query: "getAds", method: HTTPMethod.get).start(){ (response, error) in
            do{
                
                
                let object =  try JSONDecoder().decode(AdsObject.self, from: response.data!)
                self.sliderArray = object.items!
                self.pager.numberOfPages = self.sliderArray.count

                self.collectionview.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillDisappear(_ animated: Bool) { self.timer?.invalidate() }
    func autoScrollImageSlider() {
        
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                
                let firstIndex = 0
                let lastIndex = (self.sliderArray.count) - 1
                
                let visibleIndices = self.collectionview.indexPathsForVisibleItems
                let nextIndex = visibleIndices[0].row + 1
                
                let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
                let firstIndexPath: IndexPath = IndexPath.init(item: firstIndex, section: 0)
                
                if nextIndex > lastIndex {
                    
                    self.collectionview.scrollToItem(at: firstIndexPath, at: .centeredHorizontally, animated: true)
                    
                } else {
                    
                    self.collectionview.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
                    
                }
                
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sliderArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : SRAddsCELL = collectionView.dequeueCVCell(indexPath: indexPath)
        let item = sliderArray[indexPath.row]
//        let image = item["image"] as? String ?? ""
        cell.img_add.sd_custom(url: item.image!)
//        cell.img_add.setRounded(radius: 15)
//        cell.setRounded(radius: 15)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pager.currentPage  = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collectionview.frame.size.width, height: self.collectionview.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if sliderArray[indexPath.row].type == "0"{ //store
            if sliderArray[indexPath.row].link == nil{
                return
            }
            let store = StoreStruct.init(id: Int(sliderArray[indexPath.row].link!), name: nil, email: nil, profileImage: nil, mobile: nil, status: nil, type: nil, cityID: nil, lat: nil, lng: nil, address: nil, description: nil, fullDescription: nil, openingTime: nil, closingTime: nil, categoryID: nil, facebook: nil, twitter: nil, instagram: nil, deliveryCost: nil, jobID: nil, close: nil, statusNow: nil, category: nil, city: nil, jobs: nil, subCategories: nil)
            let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
            vc.StoreInfo = store
            vc.IsPress = true
            let NavigationController :CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            NavigationController.pushViewController(vc, animated: true)
            self.present(NavigationController, animated: true, completion: nil)
            return
        }
        if sliderArray[indexPath.row].type == "1"{ // product
            if sliderArray[indexPath.row].link == nil{
                return
            }
            let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
            let prod = ProductStruct.init(id: Int(sliderArray[indexPath.row].link!), userID: "", categoryID: "", price: "", priceAfterOffer: "", availability: "", status: "", delete: "", image: "", isFavourite: nil,inCart:nil,views:nil,shareLink:nil, name: "", description: "", favourite: "", category: nil, attatchments: nil, store: nil, translations: nil)
            vc.ProductInfo = prod
            vc.IsPress = true
            let NavigationController :CustomNavigationBar = AppDelegate.sb_main.instanceVC()
            NavigationController.pushViewController(vc, animated: true)
            self.present(NavigationController, animated: true, completion: nil)
            return
        }
        guard let url = URL(string: sliderArray[indexPath.row].link!) else { return }
        UIApplication.shared.open(url)
    }
    
}
class SRAddsCELL: UICollectionViewCell {
    
    @IBOutlet weak var img_add: UIImageView!
    override func awakeFromNib() {
//        img_add.setRounded(radius: 8)
    }
}
