//
//  CustomerProvileVC.swift
//  BASIT
//
//  Created by musbah on 2/27/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomerProvileVC: UIViewController {
    
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var lb_mobile: UILabel!
    @IBOutlet weak var lb_country: UILabel!
    @IBOutlet weak var lb_location: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    var userProfile : UserStruct?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Profile".localized as NSString, sender: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CurrentUser.userInfo == nil{
            let vc : LoginVC = AppDelegate.sb_main.instanceVC()
            self.present(vc , animated: true)

            return
        }
        userProfile = CurrentUser.userInfo

        imgProfile.sd_custom(url: (userProfile?.profileImage)!)
        lb_name.text = userProfile?.name
        lb_email.text = userProfile?.email
        lb_mobile.text = userProfile?.mobile
        lb_country.text = userProfile?.city?.name
        lb_location.text = userProfile?.address
        
        guard let amount = userProfile!.lat else { return }
        guard let amount2 = userProfile!.lng  else { return }

        let lat = Double(amount)
        let lang = Double(amount2)

        let coordinates = CLLocationCoordinate2DMake(lat!, lang!)
        let marker = GMSMarker(position: coordinates)
        marker.map = self.mapView
        self.mapView.animate(toLocation: coordinates)
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat!, longitude: lang!, zoom: 8.0)
        self.mapView.camera = camera

    }
    
    @IBAction func didEditePressed(_ sender: UIButton) {
        let vc:EditUserVC = AppDelegate.sb_main.instanceVC()
        vc.userProfile = self.userProfile
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
