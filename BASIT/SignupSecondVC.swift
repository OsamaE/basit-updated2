//
//  SignupVC.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps
import GooglePlaces
import GooglePlacePicker


class SignupSecondVC: SuperViewController ,SubCategoryDelgate,GMSPlacePickerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var tf_subCategory: UITextField!
    @IBOutlet weak var tf_category: UITextField!
    
    @IBOutlet weak var tf_city: UITextField!
    @IBOutlet weak var tf_Loaction: UITextField!
    @IBOutlet weak var tf_Name: UITextField!
    @IBOutlet weak var tf_Mobile: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var tf_face: UITextField!
    @IBOutlet weak var tf_twitter: UITextField!
    @IBOutlet weak var insta: UITextField!

    @IBOutlet weak var tf_Password: UITextField!
    @IBOutlet weak var tf_confirmPassword: UITextField!
    
    @IBOutlet weak var tv_shortDescription: UITextView!
    @IBOutlet weak var tv_description: UITextView!
    
    @IBOutlet weak var tf_to: UITextField!
    @IBOutlet weak var tf_from: UITextField!
    @IBOutlet weak var bt_from: UIButton!
    @IBOutlet weak var bt_to: UIButton!
    
    
    var SubCategorArray = [CategoryStruct]()
    var CategorArray = [CategoryStruct]()
    var CityArray = [CityStruct]()
    var city_id :Int!
    var lat:Float!
    var lng:Float!
    var categoryIndex :Int!
    var catIndex :Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Join Us".localized as NSString, sender: self)
        
        tv_description.placeholder = "Description"
        tv_shortDescription.placeholder = "Short Description"
        
        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "getCategories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CategoriesObject.self, from: response.data!)
                self.CategorArray = Status.Categories!
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func editImageProfile(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        self.present(vc, animated: true)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.img_Profile.image = image
        let imgData = (image.jpegData(compressionQuality: 0.1))!
        UserDefaults.standard.set(imgData, forKey: "imgData")
    }
    
    
    @IBAction func fromPickerTime(_ sender: Any) {
        
        let datePicker = ActionSheetDatePicker(title: "Time:", datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            // let rollNumber:String = String(format: "%@", value as! NSData)
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tf_from.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        //
        datePicker?.show()
    }
    
    @IBAction func toPickerTime(_ sender: Any) {
        
        let datePicker = ActionSheetDatePicker(title: "Time:", datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            // let rollNumber:String = String(format: "%@", value as! NSData)
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tf_to.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
        datePicker?.show()
    }
    
    
    @IBAction func selectLocation(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        self.tf_Loaction.text = place.formattedAddress ?? "\(place.coordinate.latitude);\(place.coordinate.longitude)"
        self.lat = Float(place.coordinate.latitude)
        self.lng = Float(place.coordinate.longitude)
        //        print("Place name \(place.name)")
        //        print("Place address \(place.formattedAddress)")
        //        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.tf_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func selectCategory(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Category".localized, rows: self.CategorArray.map { $0.title as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tf_category.text = Value as? String
            }
            self.categoryIndex = self.CategorArray[value].id
            self.catIndex = value
            print(self.CategorArray[value].id as Any)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func selectSubCategory(_ sender: Any) {
        
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpSubCategory") as! SignUpSubCategory
        vc.delegate = self
        vc.obj = self.CategorArray[catIndex].subCategory!
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func SelectedDone(_ sender: [CategoryStruct]){
        self.tf_subCategory.text = sender.map { ($0.title)! }.joined(separator:"-")
        self.SubCategorArray = sender

    }
    
    
    @IBAction func didSignUpButtonPressed(_ sender: UIButton) {
        guard Helper.isConnectedToNetwork() else {
            
            //self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        
        guard let name = self.tf_Name.text, !name.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Name required".localized)
            return
        }
        guard let email = self.tf_Email.text, !email.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Email required".localized)
            return
        }
        guard let mobile = self.tf_Mobile.text, !mobile.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Mobile required".localized)
            return
        }
        guard let password = self.tf_Password.text, !password.isEmpty else{
            self.showAlert(title:  "Erorr".localized, message: "Password required".localized)
            return
        }
        guard let confirmPassword = self.tf_confirmPassword.text, !confirmPassword.isEmpty else{
            self.showAlert(title:  "Erorr".localized, message: "Confirm Password required".localized)
            return
        }
        guard let cityID = self.city_id else{
            self.showAlert(title:  "Erorr".localized, message: "City required".localized)
            return
        }
        guard  let categoryID = self.categoryIndex else{
            self.showAlert(title: "Erorr".localized, message: "Category required".localized)
            return
        }
        if SubCategorArray.count == 0 {
            self.showAlert(title:  "Erorr".localized, message: "Sub Category required".localized)
            return

        }
        
        guard let lng = self.lng else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        guard let lat = self.lat else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        guard let address = self.tf_Loaction.text else{
            self.showAlert(title: "Erorr".localized, message: "Address required".localized)
            return
        }
        guard let openTime = self.tf_from.text, !openTime.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Open Time required".localized)
            return
        }
        guard let closeTime = self.tf_to.text, !closeTime.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Close Time required".localized)
            return
        }
        guard let fullDescription = self.tv_description.text, !fullDescription.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Description Time required".localized)
            return
        }
        guard let description = self.tv_shortDescription.text, !description.isEmpty else{
            self.showAlert(title: "Erorr".localized, message: "Description Time required".localized)
            return
        }
        guard let imgProfile = self.img_Profile.image else{
            self.showAlert(title: "Erorr".localized, message: "image Profile required".localized)
            return
        }
        
        let subcatID = self.SubCategorArray.map { ($0.id)! }
        

        self.showIndicator()
        let parameters: [String: Any] = ["name": name, "email":email, "mobile":mobile, "password":password, "confirm_password":confirmPassword, "city_id":cityID.description, "lng":lng.description, "lat":lat.description, "address":address, "opening_time":openTime, "closing_time":closeTime, "full_description":fullDescription, "description":description, "main_categories":categoryID.description, "categories" : subcatID.map { String($0) }.joined(separator:",") ,"type" :"1","device_type":"ios","fcm_token":Helper.device_token,"facebook":self.tf_face.text!,"twitter":self.tf_twitter.text!,"instagram":self.insta.text!]
        
        WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.kAPIBaseURL + "signUp", parameters: parameters as! [String : String], img:imgProfile ,withName:"img_Profile") {(response, error) in
            self.hideIndicator()

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if !Status.status! {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                CurrentUser.userInfo = Status.items
                print("SessionManager.shared.session", CurrentUser.userInfo as Any)
                if CurrentUser.userInfo != nil{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TTabBarViewController") as! TTabBarViewController
                    self.present(vc, animated: true, completion: nil)
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
        //        _ = WebRequests.setup(controller: self).prepare(query: "signUp", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
        //        }
        
    }
    
    

