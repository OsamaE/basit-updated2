//
//  StoreFilter.swift
//  BASIT
//
//  Created by ahmed on 3/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol StoreFilterDelgate : class {
    func SelectedDone(_ status: String, city: Int)
}
class StoreFilter: SuperViewController {
    @IBOutlet weak var btn_All: SelectButton!
    @IBOutlet weak var btn_open: SelectButton!
    @IBOutlet weak var btn_close: SelectButton!
    @IBOutlet  var lbl_city: UILabel!
    var delegate:StoreFilterDelgate?

    var CityArray = [CityStruct]()
    var city_id = 99
    var status = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        btn_All?.alternateButton = [btn_open!,btn_close!]
        btn_close?.alternateButton = [btn_open!,btn_All!]
        btn_open?.alternateButton = [btn_All!,btn_close!]
        btn_All.isSelected = false
        btn_close.isSelected = false
        btn_open.isSelected = false

        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Store Filter".localized as NSString, sender: self)
        _ = WebRequests.setup(controller: self).prepare(query: "getCities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.CityArray = Status.CityStruct!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: UIButton) {
        delegate?.SelectedDone(self.status, city: city_id)
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.CityArray.map { $0.name as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.lbl_city.text = Value as? String
            }
            self.city_id = self.CityArray[value].id!
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func selectPaymentMethod(_ sender: UIButton) {
        if sender.tag == 0{
            //            self.view_Delivery.isHidden = false
            self.status = "0"
        }
        if sender.tag == 1{
            self.status = "1"

            //            self.view_Delivery.isHidden = false
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
