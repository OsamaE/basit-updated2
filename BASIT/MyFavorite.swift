//
//  MyFavorite.swift
//  BASIT
//
//  Created by ahmed on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class MyFavorite: SuperViewController {
    var ProductsArray = [ProductStruct]()
    @IBOutlet  var TableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadDate()
        TableView.registerCell(id: "cartCell")
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle( ("My Favorite".localized as? NSString)!, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        // Do any additional setup after loading the view.
    }
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "myFavorites", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(ProductsObject.self, from: response.data!)
                self.ProductsArray = Status.ProductStruct!
                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyFavorite: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProductsArray.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
        return (ProductsArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = ProductsArray[indexPath.row]
        let cell: cartCell = tableView.dequeueTVCell()
        cell.lb_catogry.text = Store.store?.name!
        cell.lb_title.text = Store.name ?? ""
        cell.lb_price.text = Store.price!
        cell.img_thumb.sd_custom(url: (Store.image)!)
        
        cell.btn_remove.isHidden = true
        cell.lb_qun.isHidden = true

        cell.btn_add.isHidden = true
        cell.btn_delet.tag = indexPath.row
        cell.btn_delet.addTarget(self, action: #selector(pressCallButton(_:)), for: .touchUpInside)
        
        return cell
        
    }
    @objc func pressCallButton(_ button: UIButton) {
        
        //        let parameters: [String: Any] = ["product_id":self.cartItem?.cart![button.tag].productID! ?? 0,"type":"increase" ]
        
        let controller = UIAlertController(title:"", message: "Are you sure you want to delete?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
//            _ = WebRequests.setup(controller: self).prepare(query: "deleteProductCart/"+"\(ProductsArray[button.tag].id ?? 0)", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
//                self.loadDate()
//            }
            
            let parameters: [String: Any] = ["type": "0","product_id":"\(self.ProductsArray[button.tag].id ?? 0)" ]
            
            _ = WebRequests.setup(controller: self).prepare(query: "favorite", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
             
                                self.loadDate()

            }
        })
        let no = UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action) in
        })
        controller.addAction(yes)
        controller.addAction(no)
        self.present(controller, animated: true, completion: nil)
        
        
        
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
                vc.ProductInfo = ProductsArray[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didcheckoutPressed(_ sender: UIButton) {
        let vc:checkout = AppDelegate.sb_main.instanceVC()
//        vc.cartItem = self.cartItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
