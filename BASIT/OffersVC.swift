//
//  OffersVC.swift
//  BASIT
//
//  Created by ahmed on 3/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class OffersVC: SuperViewController ,OfferFilterDelgate{
 
    
    @IBOutlet weak var tableView: UITableView!
    var OfferArray = [ItemOffer]()
    var category_id = -99
    var priceTo = -99
    var priceFrom = -99

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Offers".localized as NSString, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        self.tableView.registerCell(id: "OffersCell")
        navigation.setRightButtons([navigation.FilterBtn as Any], sender: self)
        GetData()

        // Do any additional setup after loading the view.
    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let vc:offerFilterVC = AppDelegate.sb_main.instanceVC()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func SelectedDone(_ priceFrom: Int, priceTo: Int, category_id: Int) {
        if priceFrom > 0 {
            self.priceFrom = priceFrom
        }
        if priceTo > 0 {
            self.priceTo = priceFrom
        }
        if category_id != -99{
            self.category_id = category_id
        }
        GetData()
    }
    func GetData(){
        var link = "offers?"
        if category_id != -99{
            link = link + "category_id=\(category_id )&"
        }
        if priceTo != -99{
            link = link + "priceTo=\(self.priceTo)&"
        }
        if priceFrom != -99{
            link = link + "priceFrom=\(self.priceFrom)"
        }
        OfferArray.removeAll()
        self.tableView.reloadData()

        _ = WebRequests.setup(controller: self).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
            do{
                let object =  try JSONDecoder().decode(OfferObject.self, from: response.data!)
                self.OfferArray = object.items!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
//        _ = WebRequests.setup(controller: self).prepare(query: "offers", method: HTTPMethod.get).start(){ (response, error) in
//            do{
//
//
//                let object =  try JSONDecoder().decode(OfferObject.self, from: response.data!)
//                self.OfferArray = object.items!
//                self.tableView.reloadData()
//            } catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
//        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OffersVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.OfferArray.count == 0 {
            self.tableView.setEmptyMessage("No Data".localized)
        } else {
            self.tableView.restore()
        }
        return self.OfferArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = self.OfferArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersCell", for: indexPath) as! OffersCell
        cell.lbl_Name.text = order.name
        cell.lbl_oldPrice.text = order.price! + " " + "QAR".localized
        cell.lbl_currPrice.text = order.priceAfterOffer! + " " + "QAR".localized
        cell.imageBg.sd_custom(url: (order.image)!)

        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 253
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
        let prod = ProductStruct.init(id: self.OfferArray[indexPath.row].id, userID: "", categoryID: "", price: "", priceAfterOffer: "", availability: "", status: "", delete: "", image: "", isFavourite: nil,inCart:nil,views:nil,shareLink:nil, name: "", description: "", favourite: "", category: nil, attatchments: nil, store: nil, translations: nil)
        vc.ProductInfo = prod
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
