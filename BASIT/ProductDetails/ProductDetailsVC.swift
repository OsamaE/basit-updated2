//
//  ProductDetailsVC.swift
//  BASIT
//
//  Created by ahmed on 2/9/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import SKPhotoBrowser
class ProductDetailsVC: SuperViewController {
    @IBOutlet weak var view_pager: FSPagerView!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_category: UILabel!
    @IBOutlet weak var lbl_Subcategory: UILabel!
    @IBOutlet weak var lbl_avilibel: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_Offerprice: UILabel!
    @IBOutlet weak var lbl_priceLine: UILabel!

    @IBOutlet weak var lbl_views: UILabel!
    @IBOutlet weak var lbl_StoreName: UILabel!
    @IBOutlet weak var lbl_details: UILabel!
    @IBOutlet weak var fav_btn: UIButton!
    @IBOutlet weak var cart_btn: UIButton!

    var IsPress = false
 
    var catogerylist = [CategoryStruct]()

    var ProductInfo: ProductStruct?
    var AttatchmentArray = [Attatchment]()
    var images = [SKPhoto]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        if IsPress{
            navigation.setCustomBackButtonWithdismiss(sender: self)
        }else{
            navigation.setCustomBackButtonForViewController(sender: self)
        }
        navigation.setTitle("Product Details".localized as NSString, sender: self)
        self.setupPager()
        GetProduct()
        navigation.setRightButtons([navigation.CartBtn as Any], sender: self)

        // Do any additional setup after loading the view.
    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        self.tabBarController?.selectedIndex = 2
    }

    func GetProduct(){
        _ = WebRequests.setup(controller: self).prepare(query: "getProductDetails/" + "\(self.ProductInfo?.id ?? 0)" , method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(ProductObject.self, from: response.data!)
                self.ProductInfo = Status.ProductStruct!
                self.AttatchmentArray = (self.ProductInfo?.attatchments)!
                for item in self.AttatchmentArray{
                    let photo = SKPhoto.photoWithImageURL(item.image!)
                    photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                    self.images.append(photo)
                }
                
                self.setupPager()
                self.lbl_Subcategory.text = self.ProductInfo!.category?.title
                self.lbl_views.text = "\(self.ProductInfo?.views ?? 0)"
                self.lbl_title.text = self.ProductInfo?.name
                self.lbl_category.text = self.ProductInfo!.store?.category?.title
                
                self.lbl_title.text = self.ProductInfo?.name
                if self.ProductInfo?.availability == "1"{
                    self.lbl_avilibel.text = "YES".localized
                }else{
                    self.lbl_avilibel.text = "NO".localized
                    
                }
                if self.ProductInfo?.priceAfterOffer != nil{
                    self.lbl_price.text =  (self.ProductInfo?.price)! + " " + "QAR".localized
                    self.lbl_Offerprice.text =  (self.ProductInfo?.priceAfterOffer)! + " " + "QAR".localized

                }else{
                    self.lbl_price.text =  (self.ProductInfo?.price)! + " " + "QAR".localized
                    self.lbl_Offerprice.isHidden = true
                    self.lbl_priceLine.isHidden = true
                }
                self.lbl_price.text =  (self.ProductInfo?.price)! + " " + "QAR".localized
                self.lbl_StoreName.text = self.ProductInfo?.store?.name
                self.lbl_details.text = self.ProductInfo?.description
                if self.ProductInfo?.isFavourite == 1
                    
                {
                    self.fav_btn.setImage(UIImage(named: "ic_fav"), for: UIControl.State.normal)
                    self.fav_btn.isSelected = true
                    
                }else{
                   self.fav_btn.isSelected = false
                    
                }
                if self.ProductInfo?.inCart == 1 {
//                    self.cart_btn.isHidden = true
                }
//                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    @IBAction func ShareButton(_ button: UIButton) {
        let message = self.ProductInfo?.name!
        //Set the link to share.
//        if let url = NSURL(string: (self.ProductInfo?.shareLink!)!) {
//            UIApplication.shared.openURL(url as URL)
//        }
//        guard let url = URL(string: (self.ProductInfo?.shareLink)!) else { return }
        let link = (self.ProductInfo?.shareLink)!
//        let url = NSURL(string : link.fixedArabicURL!)

//        UIApplication.shared.open(URL(string: link.fixedArabicURL!)!)

//        {
        let objectsToShare = [message ?? "",link.fixedArabicURL!] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    @IBAction func pressCallButton(_ button: UIButton) {
        if CurrentUser.userInfo == nil {
            let controller = UIAlertController(title: "", message: "Please Login", preferredStyle: .alert)
            let yes = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            })
            
            controller.addAction(yes)
            self.present(controller, animated: true, completion: nil)
            
            return
        }
        guard Helper.isConnectedToNetwork() else {
            
            //            self.showAlert(title: "Error".localized, message: Msg.NoInternetConnection())
            return }
        var type = 1
        if button.isSelected {
            type = 0
        }
        let parameters: [String: Any] = ["type": type,"product_id":"\(self.ProductInfo?.id! ?? 0)" ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "favorite", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                //                    self.userInfo = Status.UserStruct!
                if Status.status!{

                    if  button.isSelected{
                        self.fav_btn.setImage(UIImage(named: "ic_unfav"), for: UIControl.State.normal)

                        button.isSelected = false
                        return
                    }
                    self.fav_btn.setImage(UIImage(named: "ic_fav"), for: UIControl.State.normal)
                    
                    button.isSelected = true

                    
                }else{
                    self.showAlert(title: "Error".localized, message: Status.message!)
                    
                    
                }
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }
    
    
    @IBAction func addToCart(_ sender: UIButton) {
        let parameters: [String: Any] = ["product_id":self.ProductInfo!.id! ,"quantity":"1" ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "addProductToCart", method: HTTPMethod.post, parameters: parameters, isAuthRequired : true).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
//                if Status.status! == false {
                    self.alert(message: Status.message!)
//                }
                self.GetProduct()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
    }

}
extension ProductDetailsVC: FSPagerViewDataSource, FSPagerViewDelegate {
    
    private func setupPager(){
        
        view_pager.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        view_pager.delegate = self
        view_pager.dataSource = self
        view_pager.transformer = FSPagerViewTransformer(type: .depth)
        //view_pager.transformer = FSPagerViewTransformer(type: .zoomOut)
        view_pager.itemSize = CGSize.init(width: view.frame.size.width, height: 280)
        view_pager.isInfinite = true
        pageControl.numberOfPages = self.AttatchmentArray.count
        self.pageControl.contentHorizontalAlignment = .center
        self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        pageControl.fillColors = [.selected: "43CFD7".color, .normal: "DBDBE5".color]
        
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.AttatchmentArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if let item = self.AttatchmentArray[index].image {
            cell.imageView?.sd_custom(url: item)
        }
//        cell.imageView?.sd_custom(url: (ProductInfo?.image)!)

        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(index)
        SKPhotoBrowserOptions.displayAction = false                               // action button will be hidden
        self.present(browser, animated: true, completion: nil)


    }
//
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        print(pagerView.currentIndex)
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    
}
extension String {
    var fixedArabicURL: String?  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlHostAllowed))
    } }
