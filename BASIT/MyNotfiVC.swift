//
//  MyNotfiVC.swift
//  BASIT
//
//  Created by ahmed on 4/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//
struct NotficObject: Codable {
    let status: Bool?
    let code: Int?
    let message: String?
    let items: [NotficStruct]?
}

struct NotficStruct: Codable {
    let id: Int?
    let userId, orderId, message, createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case userId = "user_id"
        case orderId = "order_id"
        case message
        case createdAt = "created_at"
    }
}
import UIKit

class MyNotfiVC: SuperViewController {
    @IBOutlet  var TableView: UITableView!
    var ProductsArray = [NotficStruct]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadDate()
        TableView.registerCell(id: "NotfiCell")
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle( ("My Notification".localized as? NSString)!, sender: self)
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setRightButtons([navigation.DeletBtn as Any], sender: self)

        // Do any additional setup after loading the view.
    }
    override func didClickRightButton(_sender :UIBarButtonItem) {
        let controller = UIAlertController(title:"", message: "Are you sure you want to delete?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
            _ = WebRequests.setup(controller: self).prepare(query: "clearNotifications", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
                self.ProductsArray.removeAll()
                self.TableView.reloadData()

                self.loadDate()
            }
        })
        let no = UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action) in
        })
        controller.addAction(yes)
        controller.addAction(no)
        self.present(controller, animated: true, completion: nil)
        
        
    }
    func loadDate(){
        _ = WebRequests.setup(controller: self).prepare(query: "myNotifications", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(NotficObject.self, from: response.data!)
                self.ProductsArray = Status.items!
                self.TableView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyNotfiVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProductsArray.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }
        return (ProductsArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = ProductsArray[indexPath.row]
        let cell: NotfiCell = tableView.dequeueTVCell()
        cell.lbl_data.text = Store.createdAt!
        cell.lbl_title.text = Store.message!

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 102
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    @IBAction func didcheckoutPressed(_ sender: UIButton) {
        let vc:checkout = AppDelegate.sb_main.instanceVC()
        //        vc.cartItem = self.cartItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
