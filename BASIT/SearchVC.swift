//
//  SearchVC.swift
//  Bunch
//
//  Created by Mostafa on 9/6/18.
//  Copyright © 2018 Mostafa. All rights reserved.
//

import UIKit

class SearchVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    lazy var searchBar:UISearchBar = UISearchBar()
    @IBOutlet weak var btn_back: UIBarButtonItem!
    @IBOutlet weak var lbl_noData: UILabel!
    var results_array: [ItemSearch] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Search".localized
        lbl_noData.isHidden = false
        lbl_noData.text = "Search Results".localized
       
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 108
        
        
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = " Search ".localized
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
//        searchBar.barTintColor = "1C1A1B".color
//        searchBar.changeSearchBarColor(color: .white)
        searchBar.delegate = self
//        searchBar.setTextColor(color: "1C1A1B".color)
        searchBar.tintColor = .white
        tableView.tableHeaderView = searchBar
        tableView.registerCell(id: "SearchCell")
        tableView.tableFooterView = UIView()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Search".localized as NSString, sender: self)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}



extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.results_array.count == 0 {
            self.tableView.setEmptyMessage("No Data".localized)
        } else {
            self.tableView.restore()
        }
        return results_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = results_array[indexPath.row]

        let cell: SearchCell = tableView.dequeueTVCell()
        cell.lbl_Name.text = item.name
        cell.imageBg.sd_custom(url: (item.image)!)
        if item.id != 1{
            cell.lbl_Type.text = "Store".localized
        }else{
            cell.lbl_Type.text = "Product".localized

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.results_array[indexPath.row]
        if item.id != 1{
            let store = StoreStruct.init(id: self.results_array[indexPath.row].id, name: nil, email: nil, profileImage: nil, mobile: nil, status: nil, type: nil, cityID: nil, lat: nil, lng: nil, address: nil, description: nil, fullDescription: nil, openingTime: nil, closingTime: nil, categoryID: nil, facebook: nil, twitter: nil, instagram: nil, deliveryCost: nil, jobID: nil, close: nil, statusNow: nil, category: nil, city: nil, jobs: nil, subCategories: nil)
            let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
            vc.StoreInfo = store
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
            let prod = ProductStruct.init(id: self.results_array[indexPath.row].id, userID: "", categoryID: "", price: "", priceAfterOffer: "", availability: "", status: "", delete: "", image: "", isFavourite: nil,inCart:nil,views:nil,shareLink:nil, name: "", description: "", favourite: "", category: nil, attatchments: nil, store: nil, translations: nil)
            vc.ProductInfo = prod
            self.navigationController?.pushViewController(vc, animated: true)

        }

        print(item)
   
    }
    
    //"No Search Results"
    
    private func fetch_search_results_data(_title: String){
        let link = "search?text=\(_title)"
        _ = WebRequests.setup(controller: self).prepare(query: link, method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(SearchObject.self, from: response.data!)
                self.results_array = Status.items!
                self.tableView.reloadData()
                self.lbl_noData.isHidden = true

            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
//    private func fetch_search_results_data٢(_title: String){
//        guard Helper.isConnectedToNetwork() else { return }
//        Helper.showIndicator(view: self.view)
//        WebRequests.sendGetRequest(url: Constant.getSearch + "?name=\(_title)", isAuth: true) { (response, error) in
//            if error != nil { Helper.hideIndicator(view: self.view); self.showAlert(title: "Error".localized, message: error?.localizedDescription ?? ""); return }
//            Helper.hideIndicator(view: self.view)
//            guard let response = response.value as? NSDictionary else { return }
//            let status = response.value(forKey: "status") as? Bool ?? false
//            //                let message = response.value(forKey: "message") as? String ?? ""
//
//            if status {
//                let items = response.value(forKey: "items") as? NSArray ?? []
//                if items.count <= 0 {
//                    self.lbl_noData.isHidden = false
//                    self.lbl_noData.text = "No Search Results".localized
//                }else{
//                    self.lbl_noData.isHidden = true
//                }
//
//                self.results_array = []
//
//                for i in items{
//                    let i = i as? NSDictionary ?? [:]
//                    let item = OffersStruct.init(dict: i)
//                    self.results_array.append(item)
//                }
//                self.tableView.reloadData()
//            }else {
//                self.results_array = []
//                self.tableView.reloadData()
//                self.lbl_noData.isHidden = false
//                self.lbl_noData.text = "No Search Results".localized
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
}


extension SearchVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let search = searchBar.text, !search.isEmpty else { return }
        print(search)
        searchBar.endEditing(true)
        self.fetch_search_results_data(_title: search)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        self.results_array = []
        lbl_noData.isHidden = false
        lbl_noData.text = "Search Results".localized
        self.tableView.reloadData()
        //self.fetch_search_results_data(_title: "")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    
}
