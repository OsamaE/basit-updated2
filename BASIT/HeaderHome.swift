//
//  HeaderHome.swift
//  BASIT
//
//  Created by ahmed on 1/17/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import SDWebImage

protocol HeaderHomeDelegate : class {
    func swiftyHeaderHomeDidTap(_ sender: Int)
}

class HeaderHome: UICollectionReusableView ,iCarouselDataSource, iCarouselDelegate{
    var sliderArray = [sliderItem]()

    func numberOfItems(in carousel: iCarousel) -> Int {
        return sliderArray.count
    }
    
    
    func InitCarousel(sendArray :[sliderItem]) {
        
        
        carousel.delegate = self
        carousel.dataSource = self
        carousel.type = iCarouselType.linear
        self.sliderArray = sendArray
        carousel.isPagingEnabled = true
        pageControl.numberOfPages = sliderArray.count
        //        print(items)
        carousel.reloadData()
    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        delegate?.swiftyHeaderHomeDidTap(index)

    }
    
    weak var delegate: HeaderHomeDelegate?
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var optionsView: UIView!
    @IBOutlet var carousel: iCarousel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func carouselDidScroll(_ carousel: iCarousel) {
        pageControl.currentPage = carousel.currentItemIndex
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let itemView = IPSliderView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 243))
        

        itemView.sliderImg.sd_setImage(with: URL(string: sliderArray[index].image!),
                                        placeholderImage: UIImage(named: "logo"),
                                        options: .highPriority) { image, error, cacheType, imageURL in
        }

//        itemView.sliderImg.sd_setImage(with: URL.init(string: image), placeholderImage: "placeholder".toImage)

        return itemView
    }
}
