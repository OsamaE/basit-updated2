//
//  CartVC.swift
//  BASIT
//
//  Created by ahmed on 2/26/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class CartVC: SuperViewController {
    @IBOutlet  var TableView: UITableView!
    @IBOutlet  var totlPrice: UILabel!
    @IBOutlet  var viw_no: UIView!

    var cartItem : cartItems?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableView.registerCell(id: "cartCell")

        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Cart".localized as NSString, sender: self)

        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
            if CurrentUser.userInfo == nil{
                let vc : LoginVC = AppDelegate.sb_main.instanceVC()
                self.present(vc , animated: true)
                return
            }
        self.cartItem = nil
        self.TableView.reloadData()
        self.totlPrice.text = "QAR".localized

        self.viw_no.isHidden = false

      loadDate()
    }
    func loadDate(){
        self.cartItem = nil
//        self.TableView.reloadData()
//        self.totlPrice.text = "QAR".localized
        _ = WebRequests.setup(controller: self).prepare(query: "getCart", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CartObject.self, from: response.data!)
                self.cartItem = Status.items!
                if self.cartItem?.cart?.count == 0{
                    self.viw_no.isHidden = false
                }else{
                    self.viw_no.isHidden = true

                }
                self.TableView.reloadData()
                self.totlPrice.text = "\(self.cartItem?.totalPrice ?? 0.0) " + "QAR".localized
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
                self.viw_no.isHidden = false

            }
        }
    }

}

extension CartVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if cartItem.self == nil{
            return 0
        }
        if cartItem?.cart!.count == 0 {
            self.TableView.setEmptyMessage("No Data".localized)
        } else {
            self.TableView.restore()
        }

        return (cartItem?.cart?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Store = cartItem?.cart![indexPath.row]
        let cell: cartCell = tableView.dequeueTVCell()
        cell.lb_catogry.text = Store?.storeName ?? ""
        cell.lb_title.text = Store?.product?.name ?? ""
        if Store?.product?.priceAfterOffer != nil {
            cell.lb_price.text = (Store?.product?.priceAfterOffer!)! + " " + "QAR".localized
        }else{
            cell.lb_price.text = (Store?.product?.price!)! + " " + "QAR".localized
        }
            
        cell.lb_qun.text = Store?.quantity!
        cell.img_thumb.sd_custom(url: (Store?.product?.image!)!)
        
        cell.btn_delet.tag = indexPath.row
        cell.btn_delet.addTarget(self, action: #selector(pressCallButton(_:)), for: .touchUpInside)

        cell.btn_add.tag = indexPath.row
        cell.btn_add.addTarget(self, action: #selector(addCallButton(_:)), for: .touchUpInside)
        cell.btn_remove.tag = indexPath.row
        cell.btn_remove.addTarget(self, action: #selector(removeCallButton(_:)), for: .touchUpInside)

        return cell
        
    }
    @objc func pressCallButton(_ button: UIButton) {
        
//        let parameters: [String: Any] = ["product_id":self.cartItem?.cart![button.tag].productID! ?? 0,"type":"increase" ]
        
        let controller = UIAlertController(title:"", message: "Are you sure you want to delete?".localized, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Ok".localized, style: .default, handler: { (action) in
            _ = WebRequests.setup(controller: self).prepare(query: "deleteProductCart/"+"\(self.cartItem?.cart![button.tag].productID ?? "0")", method: HTTPMethod.get, isAuthRequired : true).start(){ (response, error) in
                self.loadDate()
            }
        })
        let no = UIAlertAction(title: "Cancel".localized, style: .destructive, handler: { (action) in
        })
        controller.addAction(yes)
        controller.addAction(no)
        self.present(controller, animated: true, completion: nil)

      
        
        
    }
    @objc func addCallButton(_ button: UIButton) {
        
        let parameters: [String: Any] = ["product_id":self.cartItem?.cart![button.tag].productID! ?? 0,"type":"increase" ]

        _ = WebRequests.setup(controller: self).prepare(query: "changeQuantity", method: HTTPMethod.post, parameters: parameters, isAuthRequired : true).start(){ (response, error) in
            self.loadDate()
        }
    }
    @objc func removeCallButton(_ button: UIButton) {
        let parameters: [String: Any] = ["product_id":self.cartItem?.cart![button.tag].productID! ?? 0,"type":"decrease" ]
        
        _ = WebRequests.setup(controller: self).prepare(query: "changeQuantity", method: HTTPMethod.post, parameters: parameters, isAuthRequired : true).start(){ (response, error) in
            self.loadDate()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let vc:StoreDetailsVC = AppDelegate.sb_main.instanceVC()
//        vc.StoreInfo = StoresArray[indexPath.row]
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didcheckoutPressed(_ sender: UIButton) {
        if cartItem?.cart == nil {
            return
        }
        let vc:checkout = AppDelegate.sb_main.instanceVC()
        vc.cartItem = self.cartItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    
}
