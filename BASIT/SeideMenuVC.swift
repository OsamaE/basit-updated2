//
//  ViewController.swift
//  BASIT
//
//  Created by ahmed on 1/16/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class SideMenuVC: SuperViewController {
    @IBOutlet weak var vi_CousmOrder: UIView!
    @IBOutlet weak var vi_myOrder: UIView!
    @IBOutlet weak var vi_myProduct: UIView!
    @IBOutlet weak var vi_myFavorit: UIView!
    @IBOutlet weak var vi_serProvid: UIView!
    @IBOutlet weak var vi_static: UIView!
    @IBOutlet weak var vi_offer: UIView!
    @IBOutlet weak var vi_Notifcations: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setTitle("Side Menu".localized as NSString, sender: self)
        if CurrentUser.userInfo == nil{
            self.vi_CousmOrder.isHidden = true
            self.vi_myOrder.isHidden = true
            self.vi_myProduct.isHidden = true
            self.vi_static.isHidden = true
            self.vi_myFavorit.isHidden = true
            self.vi_Notifcations.isHidden = true
//            self.vi_serProvid.isHidden = true
            
        }else{
            if CurrentUser.userInfo?.type == "0" {
                self.vi_CousmOrder.isHidden = true
                self.vi_myProduct.isHidden = true
                self.vi_static.isHidden = true

            }
            if CurrentUser.userInfo?.type == "1" { //store
//                self.vi_offer.isHidden = true

            }
            if CurrentUser.userInfo?.type == "2" { //prov
                self.vi_static.isHidden = true

                self.vi_CousmOrder.isHidden = true
                self.vi_myOrder.isHidden = true
                self.vi_myProduct.isHidden = true

            }
            self.viewWillLayoutSubviews()
        }
    }
    @IBAction func StatisiticsAction(_ sender: Any) {
        let vc:StaticVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func aboutAction(_ sender: Any) {
        let vc:InfoVC = AppDelegate.sb_main.instanceVC()
        vc.Info = CurrentUser.Setting?.aboutUs
        self.navigationController?.pushViewController(vc, animated: true)

    }
    

    @IBAction func ServicesProviderAction(_ sender: Any) {
        
        let vc:ServicesProviderVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func ContactUsAction(_ sender: Any) {
        let vc:ContactUsVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func NotificationAction(_ sender: Any) {
        let vc:MyNotfiVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func OffersAction(_ sender: Any) {
        let vc:OffersVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func settingsAction(_ sender: Any) {
        let vc:SettingsVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func MyFavoriteAction(_ sender: Any) {
        let vc:MyFavorite = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func MyOrderAction(_ sender: Any) {
        let vc:MyOrderVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func MyProductsAction(_ sender: Any) {
        let vc:MyProductVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func CustomersOrdersAction(_ sender: Any) {
        let vc:CustomerOrderVC = AppDelegate.sb_main.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    
}

