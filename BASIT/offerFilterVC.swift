//
//  offerFilterVC.swift
//  BASIT
//
//  Created by ahmed on 3/10/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol OfferFilterDelgate : class {
    func SelectedDone(_ priceFrom: Int, priceTo: Int, category_id: Int)
}

class offerFilterVC: SuperViewController {
    @IBOutlet  var lbl_catogery: UILabel!
    @IBOutlet  var tf_fromPrice: UITextField!
    @IBOutlet  var tf_toPrice: UITextField!
    var delegate:OfferFilterDelgate?
    var SubCategorArray = [CategoryStruct]()
    var Cat_id = -99

    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        navigation.setCustomBackButtonForViewController(sender: self)
        navigation.setTitle("Offers Filter".localized as NSString, sender: self)
        _ = WebRequests.setup(controller: self).prepare(query: "getSubCategories", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(CategoriesObject.self, from: response.data!)
                self.SubCategorArray = Status.Categories!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: UIButton) {
        var from = -99
        var to = -99
        if self.tf_fromPrice.text != ""{
            from = Int(self.tf_fromPrice.text!)!
        }
        if self.tf_toPrice.text != ""{
            to = Int(self.tf_toPrice.text!)!
        }
        delegate?.SelectedDone(from, priceTo: to, category_id: Cat_id)
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func selectCity(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.SubCategorArray.map { $0.title as Any }, initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                
                self.lbl_catogery.text = Value as? String
            }
            self.Cat_id = self.SubCategorArray[value].id!
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
