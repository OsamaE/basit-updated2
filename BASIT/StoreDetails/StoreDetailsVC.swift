//
//  StoreDetailsVC.swift
//  BASIT
//
//  Created by ahmed on 2/5/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import MessageUI

enum options {
    case Products
    case Details
}

class StoreDetailsVC: SuperViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var collectionView: UICollectionView!

    var StoreInfo: StoreStruct?
    var selectedOption = options.Products
    var ProductsArray = [ProductStruct]()
    var IsPress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigation = self.navigationController as! CustomNavigationBar
        if IsPress{
            navigation.setCustomBackButtonWithdismiss(sender: self)
        }else{
        navigation.setCustomBackButtonForViewController(sender: self)
        }
        navigation.setTitle("Shop Details".localized as NSString, sender: self)
        
        self.collectionView.registerCell(id: "HeaderCell")
        self.collectionView.registerCell(id: "InfoCell")
        self.collectionView.registerCell(id: "ProductCell")

//        self.collectionView.allowsSelection = false
        
        GetProducts()
        // Do any additional setup after loading the view.
    }
    
    func GetProducts(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "getStoreDetails/" + "\(StoreInfo?.id ?? 0)" , method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StoresInfoObject.self, from: response.data!)
                self.StoreInfo = Status.StoreStruct
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        _ = WebRequests.setup(controller: self).prepare(query: "getProductsStore/" + "\(StoreInfo?.id ?? 0)" , method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(ProductsObject.self, from: response.data!)
                self.ProductsArray = Status.ProductStruct!
                self.collectionView.reloadData()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width

        if indexPath.section == 0{
            if StoreInfo?.fullDescription == nil{
                    return CGSize(width: screenWidth , height: 320)

            }
            let h = (StoreInfo?.fullDescription!.height(withConstrainedWidth: screenWidth, font: .systemFont(ofSize: 17)))! + 320
            return CGSize(width: screenWidth , height: h)


        }
        if selectedOption == .Details
        {

            let size = CGSize(width: screenWidth, height: 382)
            return size
        }
        let size = CGSize(width: screenWidth/2.4, height: screenWidth/2)
        return size

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if selectedOption == .Products
        {
            
            if ProductsArray.count == 0 {
                self.collectionView.setEmptyMessage("No Data".localized)
            } else {
                self.collectionView.restore()
            }
            return ProductsArray.count
        }
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section  == 0{
        let cell: HeaderCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.lb_title.text = StoreInfo?.name
            cell.lb_Cattitle.text = StoreInfo?.category?.title
        cell.lb_des.text = StoreInfo?.fullDescription
            
            cell.img_thumb.sd_custom(url: (StoreInfo?.profileImage ?? "")!)

            if StoreInfo!.statusNow == "1" {
                cell.lb_stuts.text = "Open".localized
                cell.lb_stuts.textColor = "FFFFFF".color
                
            }
            else{
                cell.lb_stuts.text = "Close".localized
                cell.lb_stuts.textColor = "FFFFFF".color
                
            }
            if StoreInfo?.category != nil  {
                if StoreInfo?.category?.subCategory != nil  {
                    cell.CategoryArray = (StoreInfo?.category?.subCategory)!
                }
            }
        cell.CollectionView.reloadData()
        if selectedOption == .Products{
            cell.btn_details.setTitleColor("649AB8".color, for: .normal)
            cell.btn_products.setTitleColor("FFFFFF".color, for: .normal)

            cell.btn_details.buttongradient.isHidden = true
            cell.btn_products.buttongradient.isHidden = false

        }else{
            
            cell.btn_products.setTitleColor("649AB8".color, for: .normal)
            cell.btn_details.setTitleColor("FFFFFF".color, for: .normal)

            cell.btn_details.buttongradient.isHidden = false
            cell.btn_products.buttongradient.isHidden = true

            }
            
            cell.btn_details.addTarget(self, action: #selector(oneTapped(_:)), for: .touchUpInside)
            cell.btn_products.addTarget(self, action: #selector(twoTapped(_:)), for: .touchUpInside)

        return cell
        }
        if selectedOption == .Details
        {
            let cell: InfoCell = collectionView.dequeueCVCell(indexPath: indexPath)
            cell.lb_workTime.text = "\(StoreInfo?.openingTime! ?? "") " + "To".localized + " \(StoreInfo?.closingTime! ?? "")"
            cell.lb_location.text = StoreInfo?.address
            cell.lb_email.text = StoreInfo?.email
            cell.lb_mobile.text = StoreInfo?.mobile
            cell.btn_face.addTarget(self, action: #selector(openface(_:)), for: .touchUpInside)
            cell.btn_twitter.addTarget(self, action: #selector(openTwitter(_:)), for: .touchUpInside)
            cell.btn_insta.addTarget(self, action: #selector(openInsta(_:)), for: .touchUpInside)
            cell.btn_call.addTarget(self, action: #selector(CallAction(_:)), for: .touchUpInside)
            cell.btn_SendEmail.addTarget(self, action: #selector(EmailAction(_:)), for: .touchUpInside)

            return cell
        }
        let Product = self.ProductsArray[indexPath.row]

        let cell: ProductCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.lb_title.text = Product.name
        cell.lb_price.text = Product.price! + " " + "QAR".localized

        cell.img_thumb.sd_custom(url: Product.image!)

        return cell

    }
    @objc func CallAction(_ button: UIButton) {
        if let url = NSURL(string: "tel://\(StoreInfo?.mobile ?? "0")"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }

    }
    @objc func EmailAction(_ button: UIButton) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([(StoreInfo?.email)!])
        composeVC.setSubject("Message Subject")
        composeVC.setMessageBody("Message content.", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)

    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    @objc func openface(_ button: UIButton) {
        //        let link = (self.ProductInfo?.StoreInfo)!
        //        let url = NSURL(string : link.fixedArabicURL!)
        
        UIApplication.shared.open(URL(string: (StoreInfo?.facebook!.fixedArabicURL!)!)!)
    }
    @objc func openInsta(_ button: UIButton) {
        //        let link = (self.ProductInfo?.StoreInfo)!
        //        let url = NSURL(string : link.fixedArabicURL!)
        
        UIApplication.shared.open(URL(string: (StoreInfo?.instagram!.fixedArabicURL!)!)!)
    }
    @objc func openTwitter(_ button: UIButton) {
        //        let link = (self.ProductInfo?.StoreInfo)!
        //        let url = NSURL(string : link.fixedArabicURL!)
        
        UIApplication.shared.open(URL(string: (StoreInfo?.twitter!.fixedArabicURL!)!)!)
    }
    @objc func oneTapped(_ sender: Any?) {
        
        selectedOption = options.Details
        self.collectionView.reloadData()
    }
    @objc func twoTapped(_ sender: Any?) {
        
        selectedOption = options.Products
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section  == 0 || selectedOption != .Products{
        return
            
        }
        let vc:ProductDetailsVC = AppDelegate.sb_main.instanceVC()
        vc.ProductInfo = self.ProductsArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
