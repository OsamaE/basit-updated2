//
//  HeaderCell.swift
//  BASIT
//
//  Created by ahmed on 2/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class HeaderCell: UICollectionViewCell ,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_Cattitle: UILabel!
    @IBOutlet weak var lb_stuts: UILabel!
    @IBOutlet weak var img_thumb: UIImageView!
    @IBOutlet weak var lb_des: UILabel!
    @IBOutlet weak var btn_products: GradientButton!
    @IBOutlet weak var btn_details: GradientButton!

    @IBOutlet weak var CollectionView: UICollectionView!
    var CategoryArray = [CategoryStruct]()

    override func awakeFromNib() {
        super.awakeFromNib()
        CollectionView.registerCell(id: "catogeryCell")
        CollectionView.delegate = self
        CollectionView.dataSource = self

        // Initialization code
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = self.CategoryArray[indexPath.row].title!.width(withConstraintedHeight: 100, font: .systemFont(ofSize: 17)) + 20
        return CGSize(width: w , height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.CategoryArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: catogeryCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.title.text = self.CategoryArray[indexPath.row].title
        cell.layoutIfNeeded()
        
        return cell
    }
}
