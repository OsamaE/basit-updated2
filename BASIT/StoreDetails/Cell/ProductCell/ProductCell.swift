//
//  ProductCell.swift
//  BASIT
//
//  Created by ahmed on 2/7/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var img_thumb: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
