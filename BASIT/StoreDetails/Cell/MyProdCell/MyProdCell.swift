//
//  MyProdCell.swift
//  BASIT
//
//  Created by ahmed on 2/28/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class MyProdCell: UITableViewCell {
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var btn_remove: UIButton!
    @IBOutlet weak var btn_edit: UIButton!
    @IBOutlet weak var img_thumb: UIImageView!
    @IBOutlet weak var btn_qun: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
