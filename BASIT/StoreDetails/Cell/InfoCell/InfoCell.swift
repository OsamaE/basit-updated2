//
//  InfoCell.swift
//  BASIT
//
//  Created by ahmed on 2/6/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class InfoCell: UICollectionViewCell {
    @IBOutlet weak var lb_workTime: UILabel!
    @IBOutlet weak var lb_location: UILabel!
    @IBOutlet weak var lb_email: UILabel!
    @IBOutlet weak var lb_mobile: UILabel!
    @IBOutlet weak var btn_face: UIButton!
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var btn_insta: UIButton!
    @IBOutlet weak var btn_call: UIButton!
    @IBOutlet weak var btn_SendEmail: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
