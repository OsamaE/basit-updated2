//
//  cartCell.swift
//  BASIT
//
//  Created by ahmed on 2/26/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit

class cartCell: UITableViewCell {
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_price: UILabel!
    @IBOutlet weak var lb_catogry: UILabel!
    @IBOutlet weak var img_thumb: UIImageView!
    @IBOutlet weak var lb_qun: UILabel!
    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var btn_remove: UIButton!
    @IBOutlet weak var btn_delet: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
